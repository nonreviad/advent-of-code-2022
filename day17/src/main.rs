use std::collections::{HashMap, HashSet};
type Rock = (u64, u64);
type Piece = Vec<Rock>;

fn horizontal_line() -> Piece {
    vec![(0, 0), (1, 0), (2, 0), (3, 0)]
}
fn cross() -> Piece {
    vec![(1, 0), (0, 1), (1, 1), (2, 1), (1, 2)]
}
fn l() -> Piece {
    vec![(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)]
}
fn vertical_line() -> Piece {
    vec![(0, 0), (0, 1), (0, 2), (0, 3)]
}
fn square() -> Piece {
    vec![(0, 0), (1, 0), (0, 1), (1, 1)]
}

struct Board {
    rocks: HashSet<Rock>,
    next_piece_idx: usize,
    next_wind_idx: usize,
    max_height: u64,
}
impl Board {
    fn new() -> Self {
        Self {
            rocks: HashSet::new(),
            next_piece_idx: 0,
            next_wind_idx: 0,
            max_height: 0,
        }
    }
    // The 'pattern' of a current board shape is related to how the tips look like.
    fn extract_pattern(&self) -> [u64; 7] {
        let mut pattern = [0; 7];
        for x in 0..7 {
            pattern[x] = self
                .rocks
                .iter()
                .filter(|(x1, _)| *x1 == x as u64)
                .map(|(_, y)| *y)
                .max()
                .unwrap_or_else(|| 0);
        }
        let min_y = *pattern.iter().min().unwrap();
        pattern.iter_mut().for_each(|y| *y -= min_y);
        pattern
    }
    fn next_piece(&mut self) -> Piece {
        let idx = self.next_piece_idx;
        self.next_piece_idx = (self.next_piece_idx + 1) % 5;
        match idx {
            0 => horizontal_line(),
            1 => cross(),
            2 => l(),
            3 => vertical_line(),
            4 => square(),
            _ => panic!("Invalid id {}", idx),
        }
    }
    fn next_jet(&mut self, jet_pattern: &Vec<Jet>) -> Jet {
        let idx = self.next_wind_idx;
        self.next_wind_idx = (self.next_wind_idx + 1) % jet_pattern.len();
        jet_pattern[idx].clone()
    }
    fn drop_next_piece(&mut self, jet_pattern: &Vec<Jet>) {
        let mut piece = self.next_piece();
        piece.iter_mut().for_each(|rock| {
            rock.0 += 2;
            rock.1 += 4 + self.max_height;
        });
        'drop_loop: loop {
            // First try to move by jets
            match self.next_jet(jet_pattern) {
                Jet::Left => {
                    let min_x = piece.iter().map(|(x, _)| *x).min().unwrap();
                    if min_x > 0 {
                        let potential_piece: Piece =
                            piece.iter().map(|(x, y)| (x - 1, *y)).collect();
                        if potential_piece
                            .iter()
                            .all(|rock| !self.rocks.contains(rock))
                        {
                            piece = potential_piece;
                        }
                    }
                }
                Jet::Right => {
                    let max_x = piece.iter().map(|(x, _)| *x).max().unwrap();
                    if max_x < 6 {
                        let potential_piece: Piece =
                            piece.iter().map(|(x, y)| (x + 1, *y)).collect();
                        if potential_piece
                            .iter()
                            .all(|rock| !self.rocks.contains(rock))
                        {
                            piece = potential_piece;
                        }
                    }
                }
            }
            // Then drop
            let min_y = piece.iter().map(|(_, y)| *y).min().unwrap();
            if min_y == 1 {
                break 'drop_loop;
            }
            let potential_piece: Piece = piece.iter().map(|(x, y)| (*x, y - 1)).collect();
            if potential_piece
                .iter()
                .all(|rock| !self.rocks.contains(rock))
            {
                piece = potential_piece;
            } else {
                break 'drop_loop;
            }
        }
        piece.into_iter().for_each(|rock| {
            self.rocks.insert(rock);
        });
        self.max_height = self.rocks.iter().map(|(_, y)| *y).max().unwrap();
    }
}
#[derive(Clone)]
enum Jet {
    Left,
    Right,
}

impl Jet {
    fn from(ch: char) -> Self {
        match ch {
            '<' => Self::Left,
            '>' => Self::Right,
            _ => panic!("Invalid direction {}", ch),
        }
    }
}

fn read_input(input: &str) -> Vec<Jet> {
    input.chars().map(Jet::from).collect()
}

fn part_one(input: &str) -> u64 {
    let mut board = Board::new();
    let wind_pattern = read_input(input);
    (0..2022).for_each(|_| board.drop_next_piece(&wind_pattern));
    board.max_height
}
// Oh man, I love automatic hashing <3.
#[derive(Hash, PartialEq, Eq, Clone)]
struct State {
    piece_idx: usize,
    jet_pattern_idx: usize,
    pattern: [u64; 7],
}

fn part_two(input: &str) -> u64 {
    // We need to find the cyclical nature of how the height increases.
    // The parameters (AFAICT) that can be used to identify a 'state' as unique are
    // the tetris piece about to be dropped, the position within the jet pattern, and
    // the topology of the tippy top of the 'board'. This somewhat misses potential
    // issues with concavities causing an issue with the 'pattern' being unique I
    // suppose, but maybe that's actually taken into account?

    let mut board = Board::new();
    let jet_pattern = read_input(input);
    // Visited states.
    let mut known_states: HashSet<State> = HashSet::new();
    // Until we hit our cycle, keep the number of pieces dropped.
    let mut first_occurrence_of_state: HashMap<State, usize> = HashMap::new();
    let mut pieces_dropped = 0;
    let mut heights: Vec<u64> = Vec::new();
    loop {
        let old_piece_idx = board.next_piece_idx;
        let old_jet_idx = board.next_wind_idx;
        let old_height = board.max_height;
        heights.push(old_height);
        let pattern = board.extract_pattern();
        let from_state = State {
            piece_idx: old_piece_idx,
            jet_pattern_idx: old_jet_idx,
            pattern,
        };
        if known_states.contains(&from_state) {
            break;
        }
        known_states.insert(from_state.clone());
        first_occurrence_of_state.insert(from_state.clone(), pieces_dropped);

        // Proceed to next state
        board.drop_next_piece(&jet_pattern);
        pieces_dropped += 1;
    }
    // OK, so now we've discovered a cycle! The way the states repeat themselves are something like
    // s[0] -> s[1] -> ... -> s[start_of_cycle] -> s[start_of_cycle + 1] -> ... -> s[start_of_cycle + length_of_cycle - 1] -
    //                                                      ^                                                              /
    //                                                       -------------------------------------------------------------

    // How many pieces do we initially need to drop before there's a cycle starting.
    let start_of_cycle = *first_occurrence_of_state
        .get(&State {
            piece_idx: board.next_piece_idx,
            jet_pattern_idx: board.next_wind_idx,
            pattern: board.extract_pattern(),
        })
        .unwrap();
    // How many pieces do we need to drop AGAIN to end up in the same position.
    let length_of_cycle = pieces_dropped - start_of_cycle;
    // How much the stack increases in height after a full cycle.
    let height_of_full_cycle = board.max_height - heights[start_of_cycle as usize];

    let mut target_pieces = 1000000000000;

    // Assume we want to simulate beyond the start of the cycle.
    let mut height = heights[start_of_cycle];
    target_pieces -= start_of_cycle;

    // For each cycle length, add its correspondent cycle height.
    height += height_of_full_cycle * ((target_pieces / length_of_cycle) as u64);

    // Now we need to see how much more height we need to add from the start of cycle position
    target_pieces = target_pieces % length_of_cycle;
    height += heights[start_of_cycle + target_pieces] - heights[start_of_cycle];

    height
}

fn main() {
    println!("{}", part_one(include_str!("./input.txt")));
    println!("{}", part_two(include_str!("./input.txt")));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(include_str!("./test_input.txt")), 3068);
    }
    #[test]
    fn test_part_two() {
        assert_eq!(part_two(include_str!("./test_input.txt")), 1514285714288);
    }
}
