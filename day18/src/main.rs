use std::collections::{HashMap, HashSet, VecDeque};

use itertools::Itertools;
#[derive(Clone, Hash, PartialEq, Eq)]
struct Cube {
    x: i32,
    y: i32,
    z: i32,
}

impl From<&str> for Cube {
    fn from(s: &str) -> Self {
        let nums = s
            .split(',')
            .map(|ss| ss.parse::<i32>().unwrap())
            .collect::<Vec<_>>();
        if nums.len() != 3 {
            panic!("Should have 3 comma separated values!");
        }
        Self {
            x: nums[0],
            y: nums[1],
            z: nums[2],
        }
    }
}

impl Cube {
    fn touches(&self, other_cube: &Cube) -> bool {
        let deltas = vec![
            self.x - other_cube.x,
            self.y - other_cube.y,
            self.z - other_cube.z,
        ];
        deltas.iter().map(|x| x.abs()).sum::<i32>() == 1
    }
}

fn area(cubes: &Vec<Cube>) -> usize {
    let mut touch_map: HashMap<usize, Vec<usize>> = HashMap::new();
    for i in 0..cubes.len() {
        touch_map.insert(i, Vec::new());
    }
    for x in 0..cubes.len() - 1 {
        for y in x..cubes.len() {
            if cubes[x].touches(&cubes[y]) {
                touch_map.get_mut(&x).unwrap().push(y);
                touch_map.get_mut(&y).unwrap().push(x);
            }
        }
    }
    touch_map
        .values()
        .map(|neighbours| 6 - neighbours.len())
        .sum()
}

fn read_input(s: &str) -> Vec<Cube> {
    s.lines().map(Cube::from).collect()
}

fn part_one(input: &str) -> usize {
    let cubes = read_input(input);
    area(&cubes)
}

fn part_two(input: &str) -> usize {
    let cubes = read_input(input).into_iter().collect::<HashSet<_>>();
    let min_x = cubes.iter().map(|cube| cube.x).min().unwrap();
    let max_x = cubes.iter().map(|cube| cube.x).max().unwrap();
    let min_y = cubes.iter().map(|cube| cube.y).min().unwrap();
    let max_y = cubes.iter().map(|cube| cube.y).max().unwrap();
    let min_z = cubes.iter().map(|cube| cube.z).min().unwrap();
    let max_z = cubes.iter().map(|cube| cube.z).max().unwrap();
    let mut area = 0;
    let mut q: VecDeque<Cube> = VecDeque::new();
    let mut visited: HashSet<Cube> = HashSet::new();
    let cube = Cube {
        x: (min_x - 1),
        y: (min_y - 1),
        z: (min_z - 1),
    };
    q.push_back(cube.clone());
    visited.insert(cube.clone());
    while !q.is_empty() {
        let cube = q.pop_front().unwrap();
        for (x, y, z) in vec![
            (cube.x - 1, cube.y, cube.z),
            (cube.x + 1, cube.y, cube.z),
            (cube.x, cube.y - 1, cube.z),
            (cube.x, cube.y + 1, cube.z),
            (cube.x, cube.y, cube.z - 1),
            (cube.x, cube.y, cube.z + 1),
        ]
        .into_iter()
        {
            if (x < (min_x - 1))
                || (x > (max_x + 1))
                || (y < (min_y - 1))
                || (y > (max_y + 1))
                || (z < (min_z - 1))
                || (z > (max_z + 1))
            {
                continue;
            }
            if cubes.contains(&Cube { x, y, z }) {
                area += 1;
            } else if !visited.contains(&Cube { x, y, z }) {
                q.push_back(Cube { x, y, z });
                visited.insert(Cube { x, y, z });
            }
        }
    }
    area
}

fn main() {
    println!("{}", part_one(include_str!("./input.txt")));
    println!("{}", part_two(include_str!("./input.txt")));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(include_str!("./test_input.txt")), 64);
    }
    #[test]
    fn test_part_two() {
        assert_eq!(part_two(include_str!("./test_input.txt")), 58);
    }
}
