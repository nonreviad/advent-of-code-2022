use std::collections::{HashMap, VecDeque};

fn find_char(board: &Vec<Vec<u8>>, ch: u8) -> (usize, usize) {
    for i in 0..board.len() {
        for j in 0..board[i].len() {
            if board[i][j] == ch {
                return (i, j);
            }
        }
    }
    panic!("No {} found", ch);
}
fn main() {
    let mut input = include_str!("./input.txt").lines().map(|line| line.as_bytes().to_vec()).collect::<Vec<_>>();
    let src = find_char(&input, b'S');
    let dst = find_char(&input, b'E');
    let mut dists: HashMap<(usize, usize), u32> = HashMap::new();
    input[src.0][src.1] = b'a';
    input[dst.0][dst.1] = b'z';
    dists.insert(src.clone(), 0);
    let mut q: VecDeque<(usize, usize)> = VecDeque::new();
    q.push_back(src.clone());
    'bfs: loop {
        let pos = q.pop_front().unwrap();
        let node = input[pos.0][pos.1];
        let cur_dist = dists.get(&pos).unwrap().clone();

        let mut to_check: VecDeque<(usize, usize)> = VecDeque::new();
        if pos.0 > 0 {
            to_check.push_back((pos.0 - 1, pos.1));
        }
        if pos.0 < input.len() - 1 {
            to_check.push_back((pos.0 + 1, pos.1));
        }
        if pos.1 > 0 {
            to_check.push_back((pos.0, pos.1 - 1));
        }
        if pos.1 < input[pos.0].len() - 1 {
            to_check.push_back((pos.0, pos.1 + 1));
        }
        for other_pos in to_check.iter() {
            if dists.contains_key(other_pos) {
                continue;
            }
            let other_node = input[other_pos.0][other_pos.1];
            match other_node {
                x if x <= node + 1 => {
                    if *other_pos == dst {
                        println!("{}", cur_dist + 1);
                        break 'bfs;
                    }
                    dists.insert(other_pos.clone(), cur_dist + 1);
                    q.push_back(other_pos.clone());
                },
                _ => {}
            }
        }
    }
    q.clear();
    dists.clear();
    for i in 0..input.len() {
        for j in 0..input[i].len() {
            if input[i][j] == b'a' {
                dists.insert((i,j), 0);
                q.push_back((i,j));
            }
        }
    }
    'bfs2: loop {
        let pos = q.pop_front().unwrap();
        let node = input[pos.0][pos.1];
        let cur_dist = dists.get(&pos).unwrap().clone();

        let mut to_check: VecDeque<(usize, usize)> = VecDeque::new();
        if pos.0 > 0 {
            to_check.push_back((pos.0 - 1, pos.1));
        }
        if pos.0 < input.len() - 1 {
            to_check.push_back((pos.0 + 1, pos.1));
        }
        if pos.1 > 0 {
            to_check.push_back((pos.0, pos.1 - 1));
        }
        if pos.1 < input[pos.0].len() - 1 {
            to_check.push_back((pos.0, pos.1 + 1));
        }
        for other_pos in to_check.iter() {
            if dists.contains_key(other_pos) {
                continue;
            }
            if *other_pos == dst {
                println!("{}", cur_dist + 1);
                break 'bfs2;
            }
            let other_node = input[other_pos.0][other_pos.1];
            match other_node {
                x if x <= node + 1 => {
                    dists.insert(other_pos.clone(), cur_dist + 1);
                    q.push_back(other_pos.clone());
                },
                _ => {}
            }
        }
    }
}
