#![allow(non_snake_case)]
use std::collections::{HashSet, HashMap};
/*

o----------> x+
|
|
|
|
v y+
*/
#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Pos {
    x: i64,
    y: i64,
}


fn step(curr_state: HashSet<Pos>, round_id: usize) -> HashSet<Pos> {
    let mut new_state = HashSet::new();
    let mut proposal_counts: HashMap<Pos, usize> = HashMap::new();
    let mut proposals: HashMap<Pos, Pos> = HashMap::new();
    curr_state.iter().for_each(|elf| {
        let N  = Pos {x: elf.x + 0, y: elf.y - 1};
        let NE = Pos {x: elf.x + 1, y: elf.y - 1};
        let E  = Pos {x: elf.x + 1, y: elf.y + 0};
        let SE = Pos {x: elf.x + 1, y: elf.y + 1};
        let S  = Pos {x: elf.x + 0, y: elf.y + 1};
        let SW = Pos {x: elf.x - 1, y: elf.y + 1};
        let W  = Pos {x: elf.x - 1, y: elf.y + 0};
        let NW = Pos {x: elf.x - 1, y: elf.y - 1};
        if !curr_state.contains(&N) && !curr_state.contains(&NE) && !curr_state.contains(&E) && !curr_state.contains(&SE) && !curr_state.contains(&S) && !curr_state.contains(&SW) && !curr_state.contains(&W) && !curr_state.contains(&NW) {
            return;
        }
        for x in 0..4 {
            let x = (x + round_id) % 4;
            match x {
                0 => {
                    if !curr_state.contains(&N) && !curr_state.contains(&NE) && !curr_state.contains(&NW) {
                        proposals.insert(elf.clone(), N.clone());
                        let cnt = proposal_counts.get(&N).map_or(0usize, |v| *v as usize);
                        proposal_counts.insert(N.clone(), cnt + 1);
                        return;
                    }
                },
                1 => {
                    if !curr_state.contains(&S) && !curr_state.contains(&SE) && !curr_state.contains(&SW) {
                        proposals.insert(elf.clone(), S.clone());
                        let cnt = proposal_counts.get(&S).map_or(0usize, |v| *v as usize);
                        proposal_counts.insert(S.clone(), cnt + 1);
                        return;
                    }
                },
                2 => {
                    if !curr_state.contains(&W) && !curr_state.contains(&NW) && !curr_state.contains(&SW) {
                        proposals.insert(elf.clone(), W.clone());
                        let cnt = proposal_counts.get(&W).map_or(0usize, |v| *v as usize);
                        proposal_counts.insert(W.clone(), cnt + 1);
                        return;
                    }
                },
                3 => {
                    if !curr_state.contains(&E) && !curr_state.contains(&NE) && !curr_state.contains(&SE) {
                        proposals.insert(elf.clone(), E.clone());
                        let cnt = proposal_counts.get(&E).map_or(0usize, |v| *v as usize);
                        proposal_counts.insert(E.clone(), cnt + 1);
                        return;
                    }
                },
                _ => unreachable!()
            }
        }
    });
    let mut moved = false;
    curr_state.iter().for_each(|elf| {
        if let Some(proposed_new) = proposals.get(elf) {
            let cnt = proposal_counts.get(proposed_new).map_or(0usize, |v| *v as usize);
            if cnt == 1 {
                new_state.insert(proposed_new.clone());
                moved = true;
            } else {
                new_state.insert(elf.clone());    
            }
        } else {
            new_state.insert(elf.clone());
        }
    });
    if moved {
        new_state
    } else {
        HashSet::new()
    }
}

fn covered_area(curr_state: &HashSet<Pos>) -> i64 {
    let min_x = curr_state.iter().map(|pos| pos.x).min().unwrap();
    let max_x = curr_state.iter().map(|pos| pos.x).max().unwrap();
    let min_y = curr_state.iter().map(|pos| pos.y).min().unwrap();
    let max_y = curr_state.iter().map(|pos| pos.y).max().unwrap();
    (max_x - min_x + 1) * (max_y - min_y + 1) - curr_state.len() as i64
}

fn read_initial_state(s: &str) -> HashSet<Pos> {
    s.lines().enumerate().flat_map(|(y,line)| {
        line.chars().enumerate().filter(|(_x, ch)| *ch == '#').map(move |(x, _ch)| {
            Pos {x: x as i64, y: y as i64}
        })
    }).collect()
}

fn part_one(s: &str) -> i64 {
    let mut elves = read_initial_state(s);
    for round_id in 0..10 {
        elves = step(elves, round_id);
    }
    covered_area(&elves)
}

fn part_two(s: &str) -> i64 {
    let mut elves = read_initial_state(s);
    let mut round_id = 0;
    loop {
        elves = step(elves, round_id);
        if elves.is_empty() {
            break;
        }
        round_id += 1;
    }
    round_id as i64 + 1
}

fn main() {
    println!("{}", part_one(include_str!("../puzzle_input.txt")));
    println!("{}", part_two(include_str!("../puzzle_input.txt")));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_part_one() {
        assert_eq!(part_one(include_str!("../test_input.txt")), 110);
    }
    #[test]
    fn test_part_two() {
        assert_eq!(part_two(include_str!("../test_input.txt")), 20);
    }
}