use std::{ops::Add, hash::Hash, collections::HashSet, cmp::min};

enum Direction {
    Up,
    Down,
    Left,
    Right
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Pos {
    x: i32,
    y: i32
}

impl Direction {
    fn delta(&self) -> Pos {
        match self {
            Direction::Up => Pos {x:0, y: 1},
            Direction::Down => Pos {x:0, y:-1},
            Direction::Left => Pos {x:-1,y:0},
            Direction::Right => Pos {x:1, y:0},
        }
    }
}

impl From<&str> for Direction {
    fn from(s: &str) -> Self {
        match s {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "L" => Direction::Left,
            "R" => Direction::Right,
            _ => panic!("Invalid direction {}", s)
        }
    }
}

impl Pos {
    fn step(&mut self, delta: &Self) {
        self.x += delta.x;
        self.y += delta.y;
    }
}

fn exec_moves(head: &mut Pos, tail: &mut Pos, dir: &Direction, steps: i32) -> Vec<Pos> {
    let delta = dir.delta();
    (0..steps).map(|_| {
        let old_head = head.clone();
        head.step(&delta);
        if (head.x - tail.x).abs() > 1 || (head.y - tail.y).abs() > 1 {
            tail.x = old_head.x;
            tail.y = old_head.y;
        }
        tail.clone()
    }).collect()
}

fn exec_moves_10(head: &mut Pos, tails: &mut Vec<Pos>, dir: &Direction, steps: i32) -> Vec<Pos> {
    let delta = dir.delta();
    (0..steps).map(|_| {
        head.step(&delta);
        let mut prev = head.clone();
        for i in 0..tails.len() {
            let dx = prev.x - tails[i].x;
            let dy = prev.y - tails[i].y;
            if dx.abs() > 1 || dy.abs() > 1 {
                tails[i].x += min(dx.abs(), 1) * dx.signum();
                tails[i].y += min(dy.abs(), 1) * dy.signum();
            }
            prev = tails[i].clone();
        }
        tails[tails.len() - 1].clone()
    }).collect()
}
fn main() {
    let mut head = Pos {x: 0, y: 0};
    let mut tail = Pos {x: 0, y: 0};
    let input = include_str!("./input.txt");
    let mut positions = input.lines().flat_map(|line| {
        let (dir, steps) = line.split_once(' ').unwrap();
        let steps = steps.parse::<i32>().unwrap();
        let dir = Direction::from(dir);
        exec_moves(&mut head, &mut tail, &dir, steps)
    }).collect::<HashSet<_>>();
    positions.insert(Pos{x: 0, y: 0});
    println!("{}", positions.len());
    let mut head = Pos { x: 0, y: 0};
    let mut tails = (0..9).map(|_| Pos {x: 0, y:0}).collect::<Vec<_>>();
    let mut positions = input.lines().flat_map(|line| {
        let (dir, steps) = line.split_once(' ').unwrap();
        let steps = steps.parse::<i32>().unwrap();
        let dir = Direction::from(dir);
        let x = exec_moves_10(&mut head, &mut tails, &dir, steps);
        x
    }).collect::<HashSet<_>>();
    positions.insert(Pos{x: 0, y: 0});
    println!("{}", positions.len());
}
