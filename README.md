# Advent of Code 2022

This is a report of all my ~~attempts at solving~~ solutions for the 2022 [Advent of Code](https://adventofcode.com/2022). Code licensed under [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).

# Day 1

EZ Clap sum + sort and sum of last three.

# Day 2

Slighty more challenging than expected; couldn't find a quick and elegant way of doing the rock-paper-scissors representation. Still pretty EZ Clap.

# Day 3

Even more challenging; learned about chunk iterator in intertools, so that's cool!

# Day 4

Pretty much easier than all the previous days, still EZ Clap.

# Day 5

Still easy, just a bit annoying to do in Rust.
For example, I couldn't find a good way to do part two without extra memory; i.e. something like

```rust
let from_col = &surface.columns[self.from - 1];
let to_col = &mut surface.columns[self.to - 1]; // Already borrowed above
for idx in (0..self.count).rev() {
    to_col.push(from_col[from_col.len() - idx - 1]);
}
```

But I'm fine with that for the most part.

# Day 6

Still a bit easy; could be done better with a map from char to count, maybe? But there's really no point in doing a 'better' solution than the naive one.

# Day 7

OK, this is getting more interesting. We can have a lil' graph theory, as a treat.

# Day 8

This could be done easier through a DP approach, but a brute force approach is sufficient, given the size of the problem.

# Day 9

Very very annoying, because the rules are underspecified and difficult to query.

# Day 10

Ugh, super confusing dealing with edge cases, but otherwise EZ.

# Day 11

I feel dirty for the way I solved it, but it's still correct; the difference between the normal solution and part two solution is that everything is modded by the GCM of all modulo test values. Haven't computed *actual* GCM, just the product of all modulo test values, since I assume they're all different and prime.

# Day 12

Just a lil' BFS, as a treat. I was super confused because part 2 had almost the same answer as part 1.

# Day 13

Pretty cool; I'm sure more TDD could be done for this, but again, could not be arsed.

# Day 14

Did a brute force solution; it seems good enough tho :shrug:.

# Day 15

Also tried brute force (and worked!), but eventually went slightly less brute force for part one by using interval trees.
For part 2, I do a sweep in the X axis first, then find the proper Y coordinate. This could probably be done better, but this runs quick enough.

# Day 16

A bit disappointed, part 2 took 8m20s to compute. The TL;dr is that I create a 'summary' for a 'state' (i.e. what valve a person is at + open valves at that point), and a way of comparing states (current expected pressure and in case of equality, number of remaining 'moves'/minutes) and use something like Dijkstra. I assume a good choice for heuristic function would make A* tractable.

# Day 17

Interesting! Discovering the pattern for the cycle was quite a challenge, but worth it in the end! I've documented it better in the comments, I think.

# Day 18

This was heartbreaking :(. I had a clever idea of how to do it that was actually wrong, and had to resort to a basic floodfill algo.

# Day 19

I cheated a bit by just... waiting. I'm doing basically a BFS with minimal culling of 'bad' states. This could be improved further by culling even **more** wrong states (i.e. by also considering the number of minerals already mined, instead of only considering robots built).

# Day 20

Spent a whole lot of time chasing the off-by-one :(.

# Day 21

Actually relatively easy; just a lil' bit of tree manipulation. I really really wanted to be able to use no string copies, but failed :'(.