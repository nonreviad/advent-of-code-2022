#[derive(Eq, PartialEq, Clone)]
enum Move {
    Rock,
    Paper,
    Scissors,
}

#[derive(Eq, PartialEq)]
enum Result {
    Loss,
    Draw,
    Win,
}

impl Result {
    fn from_str(val: &str) -> Self {
        match val {
            "X" => Result::Loss,
            "Y" => Result::Draw,
            "Z" => Result::Win,
            _ => panic!("Unexpected pattern {}", val),
        }
    }
    fn value(&self) -> u32 {
        match self {
            Result::Loss => 0,
            Result::Draw => 3,
            Result::Win => 6,
        }
    }
}

impl Move {
    fn choose_opponent(&self, result: &Result) -> Self {
        for opponent in [Move::Rock, Move::Paper, Move::Scissors].into_iter() {
            if &opponent.result(&self) == result {
                return opponent;
            }
        }
        panic!("Could not choose opponent");
    }
    fn value(&self) -> u32 {
        match self {
            Move::Rock => 1,
            Move::Paper => 2,
            Move::Scissors => 3,
        }
    }
    fn result(&self, other: &Self) -> Result {
        match (self, other) {
            (x, y) if x == y => Result::Draw,
            (Move::Rock, Move::Paper)
            | (Move::Paper, Move::Scissors)
            | (Move::Scissors, Move::Rock) => Result::Loss,
            _ => Result::Win,
        }
    }
    fn from_str(val: &str) -> Self {
        match val {
            "A" | "X" => Self::Rock,
            "B" | "Y" => Self::Paper,
            "C" | "Z" => Self::Scissors,
            _ => panic!("Unsupported val {}", val),
        }
    }
    fn against(&self, other: &Self) -> u32 {
        self.result(other).value() + self.value()
    }
}

fn main() {
    let input = include_str!("./input.txt");
    let score_1 = input
        .lines()
        .map(|round| {
            let (elf, me) = round.split_once(' ').unwrap();
            let elf = Move::from_str(elf);
            let me = Move::from_str(me);
            me.against(&elf)
        })
        .sum::<u32>();
    println!("{}", score_1);

    let score_2 = input
        .lines()
        .map(|round| {
            let (elf, desired_result) = round.split_once(' ').unwrap();
            let elf = Move::from_str(elf);
            let me = elf.choose_opponent(&Result::from_str(desired_result));
            me.against(&elf)
        })
        .sum::<u32>();
    println!("{}", score_2);
}
