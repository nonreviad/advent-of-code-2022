fn main() {
    let input: &str = include_str!("input.txt");
    let mut elves: Vec<u32> =
        input.split("\n\n")
             .map(|lines| lines.lines()
                          .map(|line| line.parse::<u32>().unwrap())
                          .sum())
        .collect();
    elves.sort_by(|a, b| b.cmp(a));
    println!("{}", elves[0]);
    println!("{}", elves[0..3].iter().sum::<u32>());
}
