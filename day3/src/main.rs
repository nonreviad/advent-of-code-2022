use itertools::Itertools;
use std::collections::HashSet;

trait Priority {
    fn prio(&self) -> u32;
}

impl Priority for char {
    fn prio(&self) -> u32 {
        if self.is_ascii_lowercase() {
            return (*self as u32) - ('a' as u32) + 1;
        }
        return (*self as u32) - ('A' as u32) + 27;
    }
}

// Convert a line to a set of its unique priorities.
fn line_to_unique_prios(line: &str) -> HashSet<u32> {
    line.chars().map(|ch| ch.prio()).collect()
}

// Get common priorities between two lines.
fn common_prios<'a>(l1: &'a HashSet<u32>, l2: &'a HashSet<u32>) -> HashSet<u32> {
    l1.intersection(&l2).copied().collect()
}

fn main() {
    let rucksacks = include_str!("./input.txt");
    let part_one = rucksacks
        .lines()
        .map(|rucksack| {
            // Split rucksack into two compartments.
            let (comp1, comp2) = rucksack.split_at(rucksack.len() / 2);
            common_prios(&line_to_unique_prios(comp1), &line_to_unique_prios(comp2)).into_iter().sum::<u32>()
        })
        .sum::<u32>();
    let part_two: u32 = rucksacks
        .lines()
        // Split into size 3 chunks.
        .chunks(3)
        .into_iter()
        .map(|chunk| {
            chunk
                // Convert each line's characters into prioties.
                .map(line_to_unique_prios)
                // Reduce with an intersection operation.
                .reduce(|l1, l2| common_prios(&l1, &l2))
                // Need to unwrap, as reduce could be empty if performed on an empty stream I suppose.
                .unwrap()
                .into_iter()
                // Sum the chunk
                .sum::<u32>()
        })
        // Sum over all chunks.
        .sum();
    println!("{}", part_one);
    println!("{}", part_two);
}
