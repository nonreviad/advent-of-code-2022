fn read_input(s: &str) -> Vec<i64> {
    s.lines().map(|l| l.parse::<i64>().unwrap()).collect()
}
fn decrypt(input_vec: &Vec<i64>) -> Vec<i64> {
    let mut v = (0..input_vec.len()).collect::<Vec<_>>();
    input_vec.iter().enumerate().for_each(|(ii, &x)| {
        if x != 0 {
            let idx = v.iter().position(|&y| y == ii).unwrap() as i64;
            let mut new_idx = idx + x;
            new_idx = new_idx.rem_euclid((input_vec.len() - 1) as i64);

            if new_idx < idx {
                let aux = v[new_idx as usize];
                v[new_idx as usize] = v[idx as usize];
                for i in (new_idx + 2..=idx).rev() {
                    v[i as usize] = v[(i - 1) as usize];
                }
                v[(new_idx + 1) as usize] = aux;
            } else if new_idx > idx {
                let aux = v[new_idx as usize];
                v[new_idx as usize] = v[idx as usize];
                for i in idx..new_idx - 1 {
                    v[i as usize] = v[(i + 1) as usize];
                }
                v[(new_idx - 1) as usize] = aux;
            }
        }
    });
    v.iter().map(|&idx| input_vec[idx]).collect()
}

fn decrypt_x10(input_vec: &Vec<i64>) -> Vec<i64> {
    let mut v = (0..input_vec.len()).collect::<Vec<_>>();
    for _ in 0..10 {
        input_vec.iter().enumerate().for_each(|(ii, &x)| {
            if x != 0 {
                let idx = v.iter().position(|&y| y == ii).unwrap() as i64;
                let mut new_idx = idx + x;
                new_idx = new_idx.rem_euclid((input_vec.len() - 1) as i64);

                if new_idx < idx {
                    let aux = v[new_idx as usize];
                    v[new_idx as usize] = v[idx as usize];
                    for i in (new_idx + 2..=idx).rev() {
                        v[i as usize] = v[(i - 1) as usize];
                    }
                    v[(new_idx + 1) as usize] = aux;
                } else if new_idx > idx {
                    let aux = v[new_idx as usize];
                    v[new_idx as usize] = v[idx as usize];
                    for i in idx..new_idx - 1 {
                        v[i as usize] = v[(i + 1) as usize];
                    }
                    v[(new_idx - 1) as usize] = aux;
                }
            }
        });
    }
    v.iter().map(|&idx| input_vec[idx]).collect()
}

fn part_one(s: &str) -> i64 {
    let v = decrypt(&read_input(s));
    let idx = v.iter().position(|&x| x == 0).unwrap();
    v[(idx + 1000) % v.len()] + v[(idx + 2000) % v.len()] + v[(idx + 3000) % v.len()]
}

fn part_two(s: &str) -> i64 {
    let mut v = read_input(s);
    v.iter_mut().for_each(|x| *x *= 811589153);
    v = decrypt_x10(&v);
    let idx = v.iter().position(|&x| x == 0).unwrap();
    v[(idx + 1000) % v.len()] + v[(idx + 2000) % v.len()] + v[(idx + 3000) % v.len()]
}

fn main() {
    println!("{}", part_one(include_str!("./input.txt")));
    println!("{}", part_two(include_str!("./input.txt")));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_part_one() {
        assert_eq!(part_one(include_str!("./test_input.txt")), 3);
    }
    #[test]
    fn test_part_two() {
        assert_eq!(part_two(include_str!("./test_input.txt")), 1623178306);
    }
}
