#![allow(dead_code)]

use std::{
    cmp::{max, min},
    collections::HashMap,
};

enum PointType {
    OpenTile,
    SolidWall,
}
// Coords are of the type
// +---------->X
// |
// |
// |
// |
// V Y
#[derive(Hash, PartialEq, Eq, Clone)]
struct Point {
    x: i32,
    y: i32,
}
struct Trail {
    points: HashMap<Point, PointType>,
    min_x_for_y: HashMap<i32, i32>,
    max_x_for_y: HashMap<i32, i32>,
    min_y_for_x: HashMap<i32, i32>,
    max_y_for_x: HashMap<i32, i32>,
}

#[derive(Clone)]
enum Facing {
    Right,
    Down,
    Left,
    Up,
}

impl Facing {
    fn to_int(&self) -> i32 {
        match self {
            Facing::Right => 0,
            Facing::Down => 1,
            Facing::Left => 2,
            Facing::Up => 3,
        }
    }
    fn deltas(&self) -> (i32, i32) {
        match self {
            Facing::Right => (1, 0),
            Facing::Down => (0, 1),
            Facing::Left => (-1, 0),
            Facing::Up => (0, -1),
        }
    }
    fn advance(&self, from_point: &Point, trail: &Trail) -> Point {
        let (dx, dy) = self.deltas();
        let x = from_point.x + dx;
        let y = from_point.y + dy;
        let potential_to_point = Point { x, y };
        let to_point = if let Some(_) = trail.points.get(&potential_to_point) {
            potential_to_point
        } else {
            match self {
                Facing::Right => Point {
                    x: *trail.min_x_for_y.get(&y).unwrap(),
                    y,
                },
                Facing::Down => Point {
                    x,
                    y: *trail.min_y_for_x.get(&x).unwrap(),
                },
                Facing::Left => Point {
                    x: *trail.max_x_for_y.get(&y).unwrap(),
                    y,
                },
                Facing::Up => Point {
                    x,
                    y: *trail.max_y_for_x.get(&x).unwrap(),
                },
            }
        };
        if let Some(point_type) = trail.points.get(&to_point) {
            match point_type {
                PointType::OpenTile => to_point,
                PointType::SolidWall => Point {
                    x: from_point.x,
                    y: from_point.y,
                },
            }
        } else {
            panic!()
        }
    }
    fn advance_3d(&self, cube_size: usize, from_point: &Point, trail: &Trail) -> (Point, Facing) {
        let (dx, dy) = self.deltas();
        let x = from_point.x + dx;
        let y = from_point.y + dy;
        let potential_to_point = Point { x, y };
        if let Some(point_type) = trail.points.get(&potential_to_point) {
            match point_type {
                PointType::OpenTile => (potential_to_point, self.clone()),
                PointType::SolidWall => (from_point.clone(), self.clone()),
            }
        } else {
            let (new_pos, new_face) = if cube_size == 4 {
                match self {
                    Facing::Right => {
                        let y_face = y / 4;
                        match y_face {
                            0 => (Point { x: 15, y: 11 - y }, Facing::Left),
                            1 => (Point { x: 19 - y, y: 8 }, Facing::Down),
                            2 => (Point { x: 11, y: 11 - y }, Facing::Left),
                            _ => panic!(),
                        }
                    }
                    Facing::Down => {
                        let x_face = x / 4;
                        match x_face {
                            0 => (Point { x: 11 - x, y: 11 }, Facing::Up),
                            1 => (Point { x: 15, y: 11 - y }, Facing::Right),
                            2 => (Point { x: 11 - x, y: 7 }, Facing::Up),
                            3 => (Point { x: 0, y: 19 - x }, Facing::Right),
                            _ => panic!(),
                        }
                    }
                    Facing::Left => {
                        let y_face = y / 4;
                        match y_face {
                            0 => (Point { x: y + 4, y: 4 }, Facing::Down),
                            1 => (Point { x: 19 - y, y: 11 }, Facing::Up),
                            2 => (Point { x: 19 - y, y: 7 }, Facing::Up),
                            _ => panic!(),
                        }
                    }
                    Facing::Up => {
                        let x_face = x / 4;
                        match x_face {
                            0 => (Point { x: 11 - x, y: 0 }, Facing::Down),
                            1 => (Point { x: 8, y: x - 4 }, Facing::Right),
                            2 => (Point { x: 11 - x, y: 4 }, Facing::Down),
                            3 => (Point { x: 11, y: 19 - x }, Facing::Left),
                            _ => panic!(),
                        }
                    }
                }
            } else {
                match self {
                    Facing::Right => {
                        let y_face = y / 50;
                        match y_face {
                            0 => (Point { x: 99, y: 149 - y }, Facing::Left), //
                            1 => (Point { x: y + 50, y:49 }, Facing::Up), //
                            2 => (Point { x: 149, y: 149 - y}, Facing::Left), //
                            3 => (Point { x: y - 100, y: 149 }, Facing::Up), //
                            _ => panic!()
                        }
                    }
                    Facing::Down => {
                        let x_face = x / 50;
                        match x_face {
                            0 => (Point { x: x + 100, y: 0 }, Facing::Down), //
                            1 => (Point { x: 49, y: x + 100 }, Facing::Left), //
                            2 => (Point { x: 99, y: x - 50 }, Facing::Left), //
                            _ => panic!(),
                        }
                    }
                    Facing::Left => {
                        let y_face = y / 50;
                        match y_face {
                            0 => (Point { x: 0, y: 149 - y }, Facing::Right), //
                            1 => (Point { x: y - 50, y: 100 }, Facing::Down), //
                            2 => (Point { x: 50, y: 149 - y }, Facing::Right), //
                            3 => (Point { x: y - 100, y:  0 }, Facing::Down), //
                            _ => panic!(),
                        }
                    }
                    Facing::Up => {
                        let x_face = x / 50;
                        match x_face {
                            0 => (Point { x: 50, y: 50 + x }, Facing::Right), //
                            1 => (Point { x: 0, y: 100 + x }, Facing::Right), //
                            2 => (Point { x: x - 100, y: 199 }, Facing::Up), //
                            _ => panic!(),
                        }
                    }
                }
            };
            if let Some(&PointType::OpenTile) = trail.points.get(&new_pos) {
                (new_pos, new_face)
            } else {
                (from_point.clone(), self.clone())
            }
        }
    }
    fn rotate_left(&self) -> Self {
        match self {
            Facing::Right => Facing::Up,
            Facing::Down => Facing::Right,
            Facing::Left => Facing::Down,
            Facing::Up => Facing::Left,
        }
    }
    fn rotate_right(&self) -> Self {
        match self {
            Facing::Right => Facing::Down,
            Facing::Down => Facing::Left,
            Facing::Left => Facing::Up,
            Facing::Up => Facing::Right,
        }
    }
}

impl From<&str> for Trail {
    fn from(s: &str) -> Self {
        let mut min_x_for_y: HashMap<i32, i32> = HashMap::new();
        let mut max_x_for_y: HashMap<i32, i32> = HashMap::new();
        let mut min_y_for_x: HashMap<i32, i32> = HashMap::new();
        let mut max_y_for_x: HashMap<i32, i32> = HashMap::new();
        let points = s
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .map(|(x, ch)| {
                        let x = x as i32;
                        let y = y as i32;
                        match ch {
                            '.' | '#' => {
                                let point = Point {
                                    x: x as i32,
                                    y: y as i32,
                                };
                                let point_type = if ch == '.' {
                                    PointType::OpenTile
                                } else {
                                    PointType::SolidWall
                                };
                                if let Some(min_x) = min_x_for_y.get_mut(&y) {
                                    *min_x = min(*min_x, x);
                                } else {
                                    min_x_for_y.insert(y, x);
                                }
                                if let Some(max_x) = max_x_for_y.get_mut(&y) {
                                    *max_x = max(*max_x, x);
                                } else {
                                    max_x_for_y.insert(y, x);
                                }
                                if let Some(min_y) = min_y_for_x.get_mut(&x) {
                                    *min_y = min(*min_y, y);
                                } else {
                                    min_y_for_x.insert(x, y);
                                }
                                if let Some(max_y) = max_y_for_x.get_mut(&x) {
                                    *max_y = max(*max_y, y);
                                } else {
                                    max_y_for_x.insert(x, y);
                                }
                                Some((point, point_type))
                            }
                            _ => None,
                        }
                    })
                    .collect::<Vec<_>>()
            })
            .filter(|opt| opt.is_some())
            .map(|opt| opt.unwrap())
            .collect::<HashMap<Point, PointType>>();
        Self {
            points,
            min_x_for_y,
            max_x_for_y,
            min_y_for_x,
            max_y_for_x,
        }
    }
}

enum FacingInstruction {
    Advance(i32),
    RotateLeft,
    RotateRight,
}

struct FacingInstructionIterator<'a> {
    s: &'a str,
}

impl<'a> Iterator for FacingInstructionIterator<'a> {
    type Item = FacingInstruction;

    fn next(&mut self) -> Option<Self::Item> {
        let bytes = self.s.as_bytes();
        if self.s.is_empty() {
            None
        } else if bytes[0].is_ascii_digit() {
            let mut idx = 0;
            let mut num: i32 = 0;
            while idx < bytes.len() && bytes[idx].is_ascii_digit() {
                num = num * 10 + (bytes[idx] - b'0') as i32;
                idx += 1;
            }
            self.s = &self.s[idx..];
            Some(FacingInstruction::Advance(num))
        } else if bytes[0] == b'L' {
            self.s = &self.s[1..];
            Some(FacingInstruction::RotateLeft)
        } else if bytes[0] == b'R' {
            self.s = &self.s[1..];
            Some(FacingInstruction::RotateRight)
        } else {
            panic!("Invalid instruction")
        }
    }
}

fn part_one(s: &str) -> i32 {
    let (trail, instructions) = s.split_once("\n\n").unwrap();
    let trail = Trail::from(trail);
    let mut facing = Facing::Right;
    let min_y = trail.points.keys().map(|p| p.y).min().unwrap();
    let min_x = *trail.min_x_for_y.get(&min_y).unwrap();
    let mut point = Point { x: min_x, y: min_y };
    FacingInstructionIterator { s: instructions }.for_each(|instr| match instr {
        FacingInstruction::Advance(num) => {
            for _ in 0..num {
                point = facing.advance(&point, &trail)
            }
        }
        FacingInstruction::RotateLeft => {
            facing = facing.rotate_left();
        }
        FacingInstruction::RotateRight => {
            facing = facing.rotate_right();
        }
    });
    (point.y + 1) * 1000 + 4 * (point.x + 1) + facing.to_int()
}
fn part_two(s: &str, cube_size: usize) -> i32 {
    let (trail, instructions) = s.split_once("\n\n").unwrap();
    let trail = Trail::from(trail);
    let mut facing = Facing::Right;
    let min_y = trail.points.keys().map(|p| p.y).min().unwrap();
    let min_x = *trail.min_x_for_y.get(&min_y).unwrap();
    let mut point = Point { x: min_x, y: min_y };
    FacingInstructionIterator { s: instructions }.for_each(|instr| match instr {
        FacingInstruction::Advance(num) => {
            for _ in 0..num {
                (point, facing) = facing.advance_3d(cube_size, &point, &trail);
            }
        }
        FacingInstruction::RotateLeft => {
            facing = facing.rotate_left();
        }
        FacingInstruction::RotateRight => {
            facing = facing.rotate_right();
        }
    });
    (point.y + 1) * 1000 + 4 * (point.x + 1) + facing.to_int()
}
fn main() {
    println!("{}", part_one(include_str!("./input.txt")));
    println!("{}", part_two(include_str!("./input.txt"), 50));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_part_one() {
        assert_eq!(part_one(include_str!("./test_input.txt")), 6032);
    }
    #[test]
    fn test_part_two() {
        assert_eq!(part_two(include_str!("./test_input.txt"), 4), 5031);
    }
}
