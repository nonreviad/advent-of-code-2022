use std::collections::HashMap;

struct Dir {
    dirs: Vec<String>,
    files: Vec<String>,
}

impl Dir {
    fn new() -> Self {
        Self {
            dirs: vec![],
            files: vec![],
        }
    }
}

struct FileSystem {
    dirs: HashMap<String, Dir>,
    files: HashMap<String, u32>,
}

impl FileSystem {
    fn new() -> Self {
        Self {
            dirs: HashMap::new(),
            files: HashMap::new(),
        }
    }
    fn add_file(&mut self, name: &String, size: u32) {
        self.files.insert(name.clone(), size);
    }
    fn add_folder(&mut self, name: &String, dir: Dir) {
        self.dirs.insert(name.clone(), dir);
    }
    fn compute_sizes(&self) -> HashMap<String, u32> {
        let mut sizes = HashMap::new();
        self.compute_impl("/", &mut sizes);
        return sizes;
    }
    fn compute_impl(&self, dir: &str, sizes: &mut HashMap<String, u32>) {
        if let Some(d) = self.dirs.get(dir) {
            d.dirs
                .iter()
                .for_each(|other_dir| self.compute_impl(other_dir, sizes));
            let sz = d
                .files
                .iter()
                .map(|file| self.files.get(file).unwrap())
                .sum::<u32>()
                + d.dirs
                    .iter()
                    .map(|other_dir| sizes.get(other_dir).unwrap())
                    .sum::<u32>();
            sizes.insert(dir.to_string(), sz);
        }
    }
}

fn main() {
    let input = include_str!("./input.txt");
    let mut lines = input.lines().peekable();
    let mut cur_dir = String::from("");
    let mut fs = FileSystem::new();
    loop {
        if let Some(line) = lines.next() {
            match line {
                "$ ls" => {
                    let mut d = Dir::new();
                    loop {
                        if let Some(line) = lines.peek() {
                            if line.starts_with("$") {
                                break;
                            }
                            let (size_or_dir, name) = line.split_once(' ').unwrap();
                            let mut full_path = format!("{}/{}", cur_dir, name);
                            if cur_dir.eq("/") {
                                full_path = format!("/{}", name);
                            }
                            match size_or_dir {
                                "dir" => {
                                    d.dirs.push(full_path.clone());
                                }
                                _ => {
                                    d.files.push(full_path.clone());
                                    fs.add_file(&full_path, size_or_dir.parse::<u32>().unwrap());
                                }
                            }
                            lines.next();
                        } else {
                            break;
                        }
                    }
                    fs.add_folder(&cur_dir, d);
                }
                "$ cd .." => {
                    let (dir, _) = cur_dir.rsplit_once('/').unwrap();
                    if dir.is_empty() {
                        cur_dir = String::from("/");
                    } else {
                        cur_dir = String::from(dir);
                    }
                }
                "$ cd /" => {
                    cur_dir = String::from("/");
                }
                _ => {
                    let (_, dir) = line.rsplit_once(' ').unwrap();
                    if cur_dir.eq("/") {
                        cur_dir = format!("/{}", dir);
                    } else {
                        cur_dir = format!("{}/{}", cur_dir, dir);
                    }
                }
            }
        } else {
            break;
        }
    }
    let sizes = fs.compute_sizes();
    let part_one = sizes.iter().filter(|(_, sz)| **sz <= 100000).map(|(_, sz)| sz).sum::<u32>();
    println!("{}", part_one);
    let to_delete = 30000000 + sizes.get("/").unwrap() - 70000000;
    println!("Must delete at least {}", to_delete);
    let x = sizes.values().filter(|x| **x >= to_delete).min().unwrap();
    println!("{}", x);
}
