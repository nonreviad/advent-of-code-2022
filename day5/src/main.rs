#[derive(Clone)]
struct Surface {
    columns: Vec<Vec<u8>>
}

impl From<&str> for Surface {
    fn from(val: &str) -> Self {
        let lines: Vec<&str> = val.split('\n').collect();
        let last_line = lines[lines.len() - 1];
        let num_columns = last_line.split(' ').filter(|x| x.len() > 0).count();
        let mut columns = Vec::new();
        for _ in 0..num_columns {
            columns.push(Vec::new());
        }
        for idx in (0..(lines.len() - 1)).rev() {
            for x in 0..num_columns {
                let ch = lines[idx].as_bytes()[x * 4 + 1];
                if ch.is_ascii_alphabetic() {
                    columns[x].push(ch);
                }
            }
        }
        Self {columns}
    }
}

impl Surface {
    fn print_tops(&self) {
        let val: Vec<u8> = self.columns.iter().map(|col| col[col.len() - 1]).collect();
        let s = String::from_utf8(val).unwrap();
        println!("{}", s);
        
    }
}

struct Move {
    from: usize,
    to: usize,
    count: usize,
}
impl Move {
    fn execute(&self, surface: &mut Surface) {
        for _ in 0..self.count {
            let val = surface.columns[self.from - 1].pop();
            if let Some(x) = val {
                surface.columns[self.to - 1].push(x);
            }
        }
    }
    fn execute_differently(&self, surface: &mut Surface) {
        let col = &mut surface.columns[self.from - 1];
        let mut aux = Vec::new();
        for _ in 0..self.count {
            aux.push(col.pop().expect("Popping off an empty stack :("));
        }
        let col = &mut surface.columns[self.to - 1];
        for _ in 0..self.count {
            let val = aux.pop().expect("Popping off an empty stack :(");
            col.push(val);
        }
    }
}
impl From<&str> for Move {
    fn from(val: &str) -> Self {
        let cols: Vec<&str> = val.split(' ').collect();
        // Could use regex, but can't be arsed.
        let count = cols[1].parse::<usize>().unwrap();
        let from  = cols[3].parse::<usize>().unwrap();
        let to    = cols[5].parse::<usize>().unwrap();
        Self { from, to, count }
    }
}

fn main() {
    let input = include_str!("./input.txt");
    let (stacks, commands) = input.split_once("\n\n").expect("Invalid input");
    let mut surface = Surface::from(stacks);
    let mut cloned_surface = surface.clone();
    let commands: Vec<_> = commands.lines().map(Move::from).collect();
    commands.iter().for_each(|command| command.execute(&mut surface));
    commands.iter().for_each(|command| command.execute_differently(&mut cloned_surface));
    surface.print_tops();
    cloned_surface.print_tops();
}
