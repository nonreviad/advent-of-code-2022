use itertools::Itertools;

fn main() {
    let forest_map: Vec<Vec<u8>> = include_str!("./input.txt")
        .lines()
        .map(|line| {
            line.chars()
                .map(|ch| ch.to_digit(10).unwrap() as u8)
                .collect()
        })
        .collect();
    let sz = forest_map.len();
    let part_one = (0..sz).cartesian_product(0..sz).filter(|(row, col)| {
        let row = *row;
        let col = *col;
        if row == 0 || col == 0 || row == sz - 1 || col == sz - 1 {
            return true;
        } else {
            let val = forest_map[row][col];
            let mut valid = true;
            // Above
            for other_row in 0..row {
                if forest_map[other_row][col] >= val {
                    valid = false;
                    break;
                } 
            }
            if valid {
                return true;
            }
            // Below
            valid = true;
            for other_row in row + 1..sz {
                if forest_map[other_row][col] >= val {
                    valid = false;
                    break;
                } 
            }
            if valid {
                return true;
            }
            // Left
            valid = true;
            for other_col in 0..col {
                if forest_map[row][other_col] >= val {
                    valid = false;
                    break;
                } 
            }
            if valid {
                return true;
            }
            // Right
            valid = true;
            for other_col in col + 1..sz {
                if forest_map[row][other_col] >= val {
                    valid = false;
                    break;
                }
            }
            if valid {
                return true;
            }
            return false;
        }
    }).count();
    println!("{}", part_one);
    let part_two = (1..sz-1).cartesian_product(1..sz-1).map(|(row, col)| {
        let val = forest_map[row][col];
        let mut result = 1;
        let mut count = 0;
        // Above
        for other_row in 1..=row {
            count += 1;
            if forest_map[row - other_row][col] >= val {
                break;
            } 
        }
        result *= count;
        // Below
        count = 0;
        for other_row in row + 1..sz {
            count += 1;
            if forest_map[other_row][col] >= val {
                break;
            }
        }
        result *= count;
        // Left
        count = 0;
        for other_col in 1..=col {
            count += 1;
            if forest_map[row][col - other_col] >= val {
                break;
            }
        }
        result *= count;
        // Right
        count = 0;
        for other_col in col + 1..sz {
            count += 1;
            if forest_map[row][other_col] >= val {
                break;
            }
        }
        result *= count;
        result
    }).max().unwrap();
    println!("{}", part_two);
}
