use std::collections::VecDeque;

enum Command {
    AddX(i32),
    Noop,
}
impl From<&str> for Command {
    fn from(line: &str) -> Self {
        if line.eq("noop") {
            return Command::Noop;
        }
        let (_, val) = line.split_once(' ').unwrap();
        return Command::AddX(val.parse::<i32>().unwrap());
    }
}

struct Cpu {
    x: i32,
    ip: i32,
    queue: VecDeque<(i32, i32)>,
}

impl Cpu {
    fn new() -> Self {
        let mut vd = VecDeque::new();
        vd.push_back((1,1));
        Self {
            x: 1,
            queue: vd,
            ip: 1,
        }
    }
    fn queue_cmd(&mut self, cmd: &Command) {
        let (ip, x) = if let Some((ip, x)) = self.queue.back() {
            (*ip, *x)
        } else {
            (self.ip, self.x)
        };
        self.queue.push_back((ip + 1, x));
        match cmd {
            Command::AddX(delta) => {
                self.queue.push_back((ip + 2, x + delta));
            }
            Command::Noop => {}
        }
    }
    fn queue_multiple(&mut self, cmds: &[Command]) {
        cmds.iter().for_each(|cmd| self.queue_cmd(cmd));
    }
}

impl Iterator for Cpu {
    type Item = (i32, i32);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((ip, x)) = self.queue.pop_front() {
            self.ip = ip;
            self.x = x;
            Some((ip, x))
        } else {
            None
        }
    }
}

fn main() {
    let cmds = include_str!("./input.txt")
        .lines()
        .map(Command::from)
        .collect::<Vec<_>>();
    let mut cpu = Cpu::new();
    cpu.queue_multiple(&cmds);
    let part_one = cpu
        .filter(|(ip, _)| *ip >= 20 && (*ip - 20) % 40 == 0)
        .map(|(ip, x)| ip * x)
        .sum::<i32>();
    println!("{}", part_one);
    println!();
    let mut cpu = Cpu::new();
    cpu.queue_multiple(&cmds);
    cpu.for_each(|(ip, x)| {
        let pos = (ip - 1) % 40;
        if pos < (x - 1) || pos > (x + 1) {
            print!(" ");
        } else {
            print!("#");
        }
        if pos == 39 {
            println!();
        }
    });
    println!();
}
