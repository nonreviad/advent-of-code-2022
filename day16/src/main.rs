#![allow(dead_code)]
#![allow(unused_variables)]

use std::collections::{ BinaryHeap, HashMap};

use lazy_regex::{lazy_regex, Lazy, Regex};

static RE: Lazy<Regex> = lazy_regex!(
    r"Valve (?P<valve_name>[A-Z]+) has flow rate=(?P<flow_rate>\d+); tunnels? leads? to valves? (?P<leads>[A-Z]+(, [A-Z]+)*)"
);
enum ValveState {
    On,
    Off,
}

struct Valve {
    name: String,
    index: u32,
    flow_rate: u32,
    leads: Vec<String>,
}
#[derive(PartialEq, Eq)]
struct PartialSolution {
    curr_position: String,
    open_valves: u64,
    curr_pressure: u32,
    remaining_steps: u32,
    // A hash of sorts of the current partial solution
    summary: u128,
}

impl Ord for PartialSolution {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let c1 = self.remaining_steps.cmp(&other.remaining_steps);
        if c1 == std::cmp::Ordering::Equal {
            return self.curr_pressure.cmp(&other.curr_pressure);
        }
        c1
    }
}

impl PartialOrd for PartialSolution {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialSolution {
    fn new(
        curr_position: String,
        curr_idx: u32,
        open_valves: u64,
        curr_pressure: u32,
        remaining_steps: u32,
    ) -> Self {
        let summary = ((curr_idx as u128) << 64) | (open_valves as u128);
        Self {
            curr_position,
            open_valves,
            curr_pressure,
            remaining_steps,
            summary,
        }
    }
    fn expand(
        self,
        q: &mut BinaryHeap<PartialSolution>,
        valves: &HashMap<String, Valve>,
        checked_solutions: &mut HashMap<u128, u32>,
    ) {
        if self.remaining_steps == 0 {
            return;
        }

        let summary = self.summary;
        // Check if a previous solution that had the same 'end' position and opened the same valves ended up with a higher pressure.
        if let Some(previous_solution) = checked_solutions.get(&summary) {
            if *previous_solution >= self.curr_pressure {
                return;
            }
        }
        checked_solutions.insert(summary, self.curr_pressure);
        let valve = valves.get(&self.curr_position).unwrap();
        if valve.flow_rate > 0
            && (self.open_valves & (1 << valve.index) == 0)
            && self.remaining_steps > 1
        {
            let mut open_valves = self.open_valves.clone();
            open_valves |= 1 << valve.index;
            let valve = valves.get(&self.curr_position).unwrap();
            let new_potential_state = PartialSolution::new(
                self.curr_position.clone(),
                valve.index,
                open_valves,
                self.curr_pressure + valve.flow_rate * (self.remaining_steps - 1),
                self.remaining_steps - 1,
            );
            q.push(new_potential_state);
        }
        valve.leads.iter().for_each(|other_valve| {
            let valve = valves.get(other_valve).unwrap();
            let new_potential_state = PartialSolution::new(
                other_valve.clone(),
                valve.index,
                self.open_valves.clone(),
                self.curr_pressure,
                self.remaining_steps - 1,
            );
            q.push(new_potential_state);
        })
    }
}

#[derive(PartialEq, Eq)]
struct PartialSolutionV2 {
    curr_position_person: String,
    curr_position_elephant: String,
    curr_idx: u32,
    curr_other_idx: u32,
    open_valves: u64,
    curr_pressure: u32,
    remaining_steps: u32,
    // A hash of sorts of the current partial solution
    summary: u128,
}

impl Ord for PartialSolutionV2 {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let c1 = self.remaining_steps.cmp(&other.remaining_steps);
        if c1 == std::cmp::Ordering::Equal {
            return self.curr_pressure.cmp(&other.curr_pressure);
        }
        c1
    }
}

impl PartialOrd for PartialSolutionV2 {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialSolutionV2 {
    fn new(
        curr_position_person: String,
        curr_position_elephant: String,
        curr_idx: u32,
        curr_other_idx: u32,
        open_valves: u64,
        curr_pressure: u32,
        remaining_steps: u32,
    ) -> Self {
        let summary =
             (1 as u128) << (curr_idx + 64) | (1 as u128) << (curr_other_idx + 64)  | (open_valves as u128);
        Self {
            curr_position_person,
            curr_position_elephant,
            curr_idx,
            curr_other_idx,
            open_valves,
            curr_pressure,
            remaining_steps,
            summary,
        }
    }
    fn expand(
        self,
        final_state: u64,
        q: &mut BinaryHeap<PartialSolutionV2>,
        valves: &HashMap<String, Valve>,
        checked_solutions: &mut HashMap<u128, u32>,
    ) {
        if self.remaining_steps == 0 || self.open_valves == final_state {
            return;
        }

        let summary = self.summary;
        // Check if a previous solution that had the same 'end' position and opened the same valves ended up with a higher pressure.
        if let Some(previous_solution) = checked_solutions.get(&summary) {
            if *previous_solution >= self.curr_pressure {
                return;
            }
        }
        checked_solutions.insert(summary, self.curr_pressure);
        let valve_person = valves.get(&self.curr_position_person).unwrap();
        let valve_elephant = valves.get(&self.curr_position_elephant).unwrap();
        // First pass, only consider where the person goes.
        let mut first_pass = Vec::new();
        if (self.open_valves & (1 << valve_person.index)) == 0 && self.remaining_steps > 1 && valve_person.flow_rate > 0 {
            let new_pressure =
                self.curr_pressure + (self.remaining_steps - 1) * valve_person.flow_rate;
            first_pass.push(PartialSolutionV2::new(
                self.curr_position_person.clone(),
                self.curr_position_elephant.clone(),
                valve_person.index,
                valve_elephant.index,
                self.open_valves | (1 << valve_person.index),
                new_pressure,
                self.remaining_steps - 1,
            ));
        }
        for neighbouring_valve in valve_person.leads.iter() {
            let neighbouring_valve = valves.get(neighbouring_valve).unwrap();
            first_pass.push(PartialSolutionV2::new(
                neighbouring_valve.name.clone(),
                self.curr_position_elephant.clone(), // will be overwritten
                neighbouring_valve.index,
                valve_elephant.index, // will be overwritten
                self.open_valves,
                self.curr_pressure,
                self.remaining_steps - 1,
            ));
        }
        // Second pass, move the elephant.
        for pass in first_pass.iter() {
            if (pass.open_valves & (1 << valve_elephant.index)) == 0 && self.remaining_steps > 1 && valve_elephant.flow_rate > 0{
                let new_pressure = pass.curr_pressure + (self.remaining_steps - 1) * valve_elephant.flow_rate;
                let new_valves = pass.open_valves | (1 << valve_elephant.index);
                q.push(PartialSolutionV2::new(
                    pass.curr_position_person.clone(),
                    self.curr_position_elephant.clone(),
                    pass.curr_idx,
                    valve_elephant.index,
                    new_valves,
                    new_pressure,
                    self.remaining_steps - 1,
                ));
            }
            for neighbouring_valve in valve_elephant.leads.iter() {
                let neighbouring_valve = valves.get(neighbouring_valve).unwrap();
                q.push(PartialSolutionV2::new(
                    pass.curr_position_person.clone(),
                    neighbouring_valve.name.clone(),
                    pass.curr_idx,
                    neighbouring_valve.index,
                    pass.open_valves,
                    pass.curr_pressure,
                    self.remaining_steps - 1,
                ));
            }
        }
    }
}

impl Valve {
    fn from(index: u32, s: &str) -> Self {
        match RE.captures(s) {
            Some(captures) => {
                let name = String::from(captures.name("valve_name").unwrap().as_str());
                let flow_rate = captures
                    .name("flow_rate")
                    .unwrap()
                    .as_str()
                    .parse::<u32>()
                    .unwrap();
                let leads = captures
                    .name("leads")
                    .unwrap()
                    .as_str()
                    .split(", ")
                    .map(|s| String::from(s))
                    .collect();
                Self {
                    name,
                    index,
                    flow_rate,
                    leads,
                }
            }
            None => panic!("Line {} does not match regex", s),
        }
    }
}

fn read_input(input: &str) -> HashMap<String, Valve> {
    input
        .lines()
        .enumerate()
        .map(|(idx, line)| {
            let valve = Valve::from(idx as u32, line);
            (valve.name.clone(), valve)
        })
        .collect()
}

fn part_one(valves: &HashMap<String, Valve>) -> u32 {
    let mut q = BinaryHeap::new();
    let mut best = 0;
    let mut visited = HashMap::new();
    let aa = valves.get("AA").unwrap();
    q.push(PartialSolution::new(String::from("AA"), aa.index, 0, 0, 30));
    while let Some(state) = q.pop() {
        best = std::cmp::max(best, state.curr_pressure);
        state.expand(&mut q, &valves, &mut visited);
    }
    best
}

fn part_two(valves: &HashMap<String, Valve>) -> u32 {
    let mut q = BinaryHeap::new();
    let mut best = 0;
    let mut visited = HashMap::new();
    let aa = valves.get("AA").unwrap();
    let mut final_state: u64 = 0;
    for valve in valves.values() {
        if valve.flow_rate > 0 {
            final_state |= 1 << valve.index;
        }
    }
    q.push(PartialSolutionV2::new(String::from("AA"), String::from("AA"), aa.index, aa.index, 0, 0, 26));
    while let Some(state) = q.pop() {
        best = std::cmp::max(best, state.curr_pressure);
        state.expand(final_state, &mut q, &valves, &mut visited);
    }
    best
}

fn main() {
    let valves = read_input(include_str!("./input.txt"));
    println!("{}", part_one(&valves));
    println!("{}", part_two(&valves));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_read() {
        let valves = read_input(include_str!("./test_input.txt"));
        assert_eq!(valves.len(), 10);
    }

    #[test]
    fn test_part_one() {
        let valves = read_input(include_str!("./test_input.txt"));
        assert_eq!(part_one(&valves), 1651);
    }
    #[test]
    fn test_part_one_real() {
        let valves = read_input(include_str!("./input.txt"));
        assert_eq!(part_one(&valves), 1947);
    }

     #[test]
    fn test_part_two() {
        let valves = read_input(include_str!("./test_input.txt"));
        assert_eq!(part_two(&valves), 1707);
    }
}
