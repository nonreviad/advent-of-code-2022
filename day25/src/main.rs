
fn read_base_five_number(s: &str) -> i64 {
    let mut ret = 0i64;
    let mut p = 1i64;
    for ch in s.chars().rev() {
        ret += match ch {
            '0' => 0,
            '1' => 1,
            '2' => 2,
            '-' => -1,
            '=' => -2,
            _ => unreachable!()
        } * p;
        p *= 5;
    }
    ret
}
fn increment_next_digit(digits: &mut Vec<i64>, digit_idx: usize) {
    if digit_idx == digits.len() - 1 {
        digits.push(1);
    } else {
        digits[digit_idx + 1] += 1;
    }
}
fn normalize_base_snafu(digits: &mut Vec<i64>) {
    for idx in 0..digits.len() {
        if digits[idx] >= 5 {
            digits[idx] -= 5;
            increment_next_digit(digits, idx);
            normalize_base_snafu(digits);
            break;
        }
        if digits[idx] == 3 {
            digits[idx] = -2;
            increment_next_digit(digits, idx);
        }
        if digits[idx] == 4 {
            digits[idx] = -1;
            increment_next_digit(digits, idx);
        }
    }
}
fn to_base_snafu(number: i64) -> String {
    let mut digits = Vec::new();
    let mut n = number / 5;
    digits.push(number % 5);
    while n > 0 {
        digits.push(n % 5);
        n /= 5;
    }
    normalize_base_snafu(&mut digits);
    digits.into_iter().rev().map(|digit| {
        match digit {
            0..=2 => (digit as u8 + '0' as u8) as char,
            -1 => '-',
            -2 => '=',
            _ => unreachable!()
        }
    }).collect()
}
fn part_one(s: &str) -> String {
    let sum: i64 = s.lines().map(|line| read_base_five_number(line)).sum();
    to_base_snafu(sum)
}
fn main() {
    println!("{}", part_one(include_str!("./input.txt")));
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(include_str!("./test_input.txt")), "2=-1=0".to_owned());
    }
}
