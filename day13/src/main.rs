use std::{iter::Peekable, str::Chars};

#[derive(Debug, Clone, PartialEq, Eq, Ord)]
enum Packet {
    Scalar(i32),
    Vector(Vec<Packet>),
}

impl Packet {
    fn from_chars(chars: &mut Peekable<Chars>) -> Packet {
        match chars.peek() {
            None => Packet::Vector(Vec::new()),
            Some(&'[') => {
                chars.next();
                Packet::vector(chars)
            }
            Some(ch) if ch.is_ascii_digit() => Self::scalar(chars),
            Some(ch) => panic!("Invalid token {}", ch),
        }
    }
    fn scalar(chars: &mut Peekable<Chars>) -> Self {
        let mut val: i32 = 0;
        'scalar_loop: loop {
            match chars.peek() {
                None => break 'scalar_loop,
                Some(ch) if ch.is_ascii_digit() => {
                    val = val * 10 + (*ch as u8 - b'0') as i32;
                    chars.next();
                }
                _ => {
                    break 'scalar_loop;
                }
            }
        }
        Self::Scalar(val)
    }
    fn vector(chars: &mut Peekable<Chars>) -> Self {
        let mut items: Vec<Packet> = Vec::new();
        'vec_loop: loop {
            match chars.peek() {
                None => break 'vec_loop,
                Some(&']') => {
                    chars.next();
                    break 'vec_loop;
                }
                Some(&',') => {
                    chars.next();
                }
                _ => {
                    items.push(Self::from_chars(chars));
                }
            };
        }
        Self::Vector(items)
    }
}

impl From<&str> for Packet {
    fn from(s: &str) -> Self {
        let mut chars = s.chars().peekable();
        Self::from_chars(&mut chars)
    }
}

fn cmp_fn(left: &Packet, right: &Packet) -> i32 {
    match (left, right) {
        (Packet::Scalar(s1), Packet::Scalar(s2)) => (s1 - s2).signum(),
        (Packet::Vector(v1), Packet::Vector(v2)) => {
            let max_idx = std::cmp::min(v1.len(), v2.len());
            for i in 0..max_idx {
                let x = cmp_fn(&v1[i], &v2[i]);
                if x != 0 {
                    return x;
                }
            }
            match (v1, v2) {
                _ if v1.len() < v2.len() => -1,
                _ if v1.len() == v2.len() => 0,
                _ => 1,
            }
        }
        (Packet::Scalar(_), Packet::Vector(_)) => {
            cmp_fn(&Packet::Vector(vec![left.clone()]), right)
        }
        (Packet::Vector(_), Packet::Scalar(_)) => {
            cmp_fn(left, &Packet::Vector(vec![right.clone()]))
        }
    }
}
impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match cmp_fn(self, other) {
            x if x < 0 => Some(std::cmp::Ordering::Less),
            x if x == 0 => Some(std::cmp::Ordering::Equal),
            x if x > 0 => Some(std::cmp::Ordering::Greater),
            _ => None,
        }
    }
}

fn main() {
    let input = include_str!("./input.txt");
    let part_one = input
        .split("\n\n")
        .enumerate()
        .map(|(idx, pair_lines)| {
            let (line1_str, line2_str) = pair_lines.split_once('\n').unwrap();
            let line1 = Packet::from(line1_str);
            let line2 = Packet::from(line2_str);
            match cmp_fn(&line1, &line2) {
                x if x <= 0 => (idx + 1) as i32,
                _ => 0,
            }
        })
        .sum::<i32>();
    println!("{}", part_one);
    let mut unsorted = include_str!("./input.txt")
        .lines()
        .filter(|line| !line.is_empty())
        .map(|line| Packet::from(line))
        .collect::<Vec<_>>();
    unsorted.push(Packet::from("[[2]]"));
    unsorted.push(Packet::from("[[6]]"));
    unsorted.sort();
    println!(
        "{}",
        (unsorted
            .iter()
            .position(|x| *x == Packet::from("[[2]]"))
            .unwrap()
            + 1)
            * (unsorted
                .iter()
                .position(|x| *x == Packet::from("[[6]]"))
                .unwrap()
                + 1)
    );
}
