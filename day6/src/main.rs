use std::collections::HashSet;

fn main() {
    let input = include_str!("./input.txt");
    for i in 3..input.len() {
        let x: HashSet<_> = (&input[(i-3)..=i]).as_bytes().iter().collect();
        if x.len() == 4 {
            println!("{}", i + 1);
            break;
        }
    }
    for i in 13..input.len() {
        let x: HashSet<_> = (&input[(i-13)..=i]).as_bytes().iter().collect();
        if x.len() == 14 {
            println!("{}", i + 1);
            break;
        }
    }
}
