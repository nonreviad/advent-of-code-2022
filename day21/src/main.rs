use std::collections::HashMap;

struct Monkey{
    name: String,
    op: Op
}

impl From<& str> for Monkey {
    fn from(s: &str) -> Self {
        let (name, op) = s.split_once(": ").unwrap();
        let name = name.to_string();
        if let Ok(num) = op.parse::<i64>() {
            return Self { name: name.to_string(), op: Op::Num(num)};
        } else {
            let ops = op.split(' ').collect::<Vec<_>>();
            if ops.len() != 3 {
                panic!("Invalid ops {op}");
            }
            let m1 = ops[0].to_string();
            let m2 = ops[2].to_string();
            match ops[1] {
                "+" => Self {name, op: Op::Add{m1,m2}},
                "-" => Self {name, op: Op::Sub{m1,m2}},
                "*" => Self {name, op: Op::Mul{m1,m2}},
                "/" => Self {name, op: Op::Div{m1,m2}},
                _ => panic!("Invalid op {}", ops[1])
            }
        }
    }
}

enum Op {
    Num(i64),
    Add{m1: String, m2: String},
    Sub{m1: String, m2: String},
    Mul{m1: String, m2: String},
    Div{m1: String, m2: String}
}

impl Op {
    fn reverse_rhs(&self, val: i64, lhs: i64) -> i64 {
        match &self {
            Op::Num(_) => panic!("Cannot reverse a num"),
            Op::Add { m1: _, m2: _ } => val - lhs,
            Op::Sub { m1: _, m2: _ } => lhs - val,
            Op::Mul { m1: _, m2: _ } => val / lhs,
            Op::Div { m1: _, m2: _ } => lhs / val,
        }
    }
    fn reverse_lhs(&self, val: i64, rhs: i64) -> i64 {
        match &self {
            Op::Num(_) => panic!("Cannot reverse a num"),
            Op::Add { m1: _, m2: _ } => val - rhs,
            Op::Sub { m1: _, m2: _ } => rhs + val,
            Op::Mul { m1: _, m2: _ } => val / rhs,
            Op::Div { m1: _, m2: _ } => rhs * val,
        }
    }
}

struct Monkeys {
    monkeys: HashMap<String, Monkey>
}

impl Monkeys {
    fn deduce_all(&self) -> HashMap<String, i64> {
        self.deduce_node(&"root".to_string(), HashMap::new())
    }
    fn deduce_node(&self, node: &String, values: HashMap<String, i64>) -> HashMap<String, i64> {
        if values.get(node).is_some() {
            return values;
        }
        if let Some(monkey) = self.monkeys.get(node) {
            match &monkey.op {
                Op::Num(num) => {
                    let mut values = values;
                    values.insert(node.clone(), *num);
                    return values;
                },
                Op::Add { m1, m2 } => {
                    let values = self.deduce_node(m1, values);
                    let values = self.deduce_node(m2, values);
                    let num = *values.get(m1).unwrap() + *values.get(m2).unwrap();
                    let mut values = values;
                    values.insert(node.clone(), num);
                    return values;
                },
                Op::Sub { m1, m2 } => {
                    let values = self.deduce_node(m1, values);
                    let values = self.deduce_node(m2, values);
                    let num = *values.get(m1).unwrap() - *values.get(m2).unwrap();
                    let mut values = values;
                    values.insert(node.clone(), num);
                    return values;
                },
                Op::Mul { m1, m2 } => {
                    let values = self.deduce_node(m1, values);
                    let values = self.deduce_node(m2, values);
                    let num = *values.get(m1).unwrap() * *values.get(m2).unwrap();
                    let mut values = values;
                    values.insert(node.clone(), num);
                    return values;
                },
                Op::Div { m1, m2 } => {
                    let values = self.deduce_node(m1, values);
                    let values = self.deduce_node(m2, values);
                    let num = *values.get(m1).unwrap() / *values.get(m2).unwrap();
                    let mut values = values;
                    values.insert(node.clone(), num);
                    return values;
                },
            }
        } else {
            panic!("Unkown monkey {node}");
        }
    }
    fn reaches(&self, from_node: &String, to_node: &String) -> bool {
        let node = self.monkeys.get(from_node).unwrap();
        match &node.op {
            Op::Num(_) => false,
            Op::Add { m1, m2 } => {
                if m1 == to_node || m2 == to_node {
                    true
                } else {
                    self.reaches(&m1, to_node) || self.reaches(&m2, to_node)
                }
            },
            Op::Sub { m1, m2 } => {
                if m1 == to_node || m2 == to_node {
                    true
                } else {
                    self.reaches(&m1, to_node) || self.reaches(&m2, to_node)
                }
            },
            Op::Mul { m1, m2 } => {
                if m1 == to_node || m2 == to_node {
                    true
                } else {
                    self.reaches(&m1, to_node) || self.reaches(&m2, to_node)
                }
            },
            Op::Div { m1, m2 } => {
                if m1 == to_node || m2 == to_node {
                    true
                } else {
                    self.reaches(&m1, to_node) || self.reaches(&m2, to_node)
                }
            },
        }
    }
    fn reverse_full(&self) -> i64 {
        let root_monkey = self.monkeys.get(&"root".to_string()).unwrap();
        let mut values: HashMap<String, i64> = HashMap::new();
        match &root_monkey.op {
            Op::Num(_) => panic!("WTF?! Root should not be a num node!"),
            Op::Add { m1, m2 } | Op::Sub { m1, m2 } | Op::Mul { m1, m2 } | Op::Div { m1, m2 } => {
                let (new_root, val) = if self.reaches(&m1, &"humn".to_string()) {
                    let new_values = self.deduce_node(&m2, HashMap::new());
                    let val = *new_values.get(m2).unwrap();
                    let new_root = self.monkeys.get(m1).unwrap();
                    values.insert(m1.clone(), val);
                    (new_root, val)
                } else {
                    let new_values = self.deduce_node(&m1, HashMap::new());
                    let val = *new_values.get(m1).unwrap();
                    let new_root = self.monkeys.get(m2).unwrap();
                    values.insert(m2.clone(), val);
                    (new_root, val)
                };
                self.reverse_node(new_root, val)
            },
        }
    }
    fn reverse_node(&self, root: &Monkey, val: i64) -> i64 {
        let humn_node = String::from("humn");
        match &root.op {
            Op::Num(_) => panic!("Wat?"),
            Op::Add { m1, m2 } | Op::Sub { m1, m2 } | Op::Mul { m1, m2 } | Op::Div { m1, m2 } => {
                if m1 == &humn_node {
                    let values = self.deduce_node(m2, HashMap::new());
                    root.op.reverse_lhs(val, *values.get(m2).unwrap())
                } else if m2 == &humn_node {
                    let values = self.deduce_node(m1, HashMap::new());
                    root.op.reverse_rhs(val, *values.get(m1).unwrap())
                } else if self.reaches(m1, &humn_node) {
                    let values = self.deduce_node(m2, HashMap::new());
                    let new_val = root.op.reverse_lhs(val, *values.get(m2).unwrap());
                    self.reverse_node(self.monkeys.get(m1).unwrap(), new_val)
                } else {
                    let values = self.deduce_node(m1, HashMap::new());
                    let new_val = root.op.reverse_rhs(val, *values.get(m1).unwrap());
                    self.reverse_node(self.monkeys.get(m2).unwrap(), new_val)
                }
            },
        }
    }
}



impl From<&str> for Monkeys {
    fn from(s: &str) -> Self {
        let monkeys = s.lines().map(|line| {
            let monkey = Monkey::from(line);
            (monkey.name.clone(), monkey)
        }).collect::<HashMap<_, _>>();
        Self {monkeys}
    }
}

fn part_one(s: &str) -> i64 {
    let monkeys = Monkeys::from(s);
    let values = monkeys.deduce_all();
    *values.get(&"root".to_string()).unwrap()
}

fn part_two(s: &str) -> i64 {
    let monkeys = Monkeys::from(s);
    monkeys.reverse_full()
}

fn main() {
    println!("{}", part_one(include_str!("./input.txt")));
    println!("{}", part_two(include_str!("./input.txt")));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(include_str!("./test_input.txt")), 152);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(include_str!("./test_input.txt")), 301);
    }
}