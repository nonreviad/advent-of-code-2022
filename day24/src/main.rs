#![allow(dead_code)]
#![allow(unused_variables)]

use std::collections::{BinaryHeap, HashMap, HashSet, VecDeque};

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct Position {
    x: i64,
    y: i64,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct Blizzard {
    direction: Direction,
    pos: Position,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
enum Tile {
    Wall { pos: Position },
    Blizzard { blizzard: Blizzard },
    Empty { pos: Position },
}
impl Tile {
    fn get_pos(&self) -> Position {
        match self {
            Tile::Wall { pos } => pos.clone(),
            Tile::Blizzard { blizzard } => blizzard.pos.clone(),
            Tile::Empty { pos } => pos.clone(),
        }
    }
}
#[derive(Debug, Clone, PartialEq, Eq)]
struct Board {
    min_x: i64,
    min_y: i64,
    max_x: i64,
    max_y: i64,
    blizzards: HashSet<Blizzard>,
    start_pos: Position,
    end_pos: Position,
}

impl Board {}

fn read_board(s: &str) -> Board {
    let tiles: HashSet<_> = s
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars().enumerate().map(move |(x, ch)| {
                let x = x as i64;
                let y = y as i64;
                let pos = Position { x, y };
                match ch {
                    '#' => Tile::Wall { pos },
                    '.' => Tile::Empty { pos },
                    'v' => Tile::Blizzard {
                        blizzard: Blizzard {
                            direction: Direction::Down,
                            pos,
                        },
                    },
                    '^' => Tile::Blizzard {
                        blizzard: Blizzard {
                            direction: Direction::Up,
                            pos,
                        },
                    },
                    '<' => Tile::Blizzard {
                        blizzard: Blizzard {
                            direction: Direction::Left,
                            pos,
                        },
                    },
                    '>' => Tile::Blizzard {
                        blizzard: Blizzard {
                            direction: Direction::Right,
                            pos,
                        },
                    },
                    _ => panic!("Unknown char {ch}"),
                }
            })
        })
        .collect();
    let empties: Vec<_> = tiles
        .iter()
        .filter(|tile| match tile {
            Tile::Empty { pos } => true,
            _ => false,
        })
        .map(|tile| tile.get_pos())
        .collect();
    let start_pos = empties
        .iter()
        .min_by(|foo, bar| foo.y.cmp(&bar.y))
        .unwrap()
        .clone();
    let end_pos = empties
        .iter()
        .min_by(|foo, bar| bar.y.cmp(&foo.y))
        .unwrap()
        .clone();
    let blizzards = tiles
        .iter()
        .filter(|tile| match tile {
            Tile::Blizzard { blizzard: _ } => true,
            _ => false,
        })
        .map(|tile| match tile {
            Tile::Blizzard { blizzard } => blizzard.clone(),
            _ => panic!("Should be unreachable"),
        })
        .collect();
    let positions: Vec<_> = tiles.iter().map(|tile| tile.get_pos()).collect();
    let min_x = positions.iter().map(|pos| pos.x).min().unwrap().clone();
    let max_x = positions.iter().map(|pos| pos.x).max().unwrap().clone();
    let min_y = positions.iter().map(|pos| pos.y).min().unwrap().clone();
    let max_y = positions.iter().map(|pos| pos.y).max().unwrap().clone();

    Board {
        min_x,
        max_x,
        min_y,
        max_y,
        blizzards,
        start_pos,
        end_pos,
    }
}
fn evolve_blizzards(board: &Board, curr_blizzards: &HashSet<Blizzard>) -> HashSet<Blizzard> {
    curr_blizzards
        .iter()
        .map(|blizzard| {
            let mut pos = blizzard.pos.clone();
            match blizzard.direction {
                Direction::Up => pos.y -= 1,
                Direction::Down => pos.y += 1,
                Direction::Left => pos.x -= 1,
                Direction::Right => pos.x += 1,
            }
            if pos.x <= board.min_x {
                pos.x = board.max_x - 1;
            }
            if pos.y <= board.min_y {
                pos.y = board.max_y - 1;
            }
            if pos.x >= board.max_x {
                pos.x = board.min_x + 1;
            }
            if pos.y >= board.max_y {
                pos.y = board.min_y + 1;
            }
            Blizzard {
                direction: blizzard.direction.clone(),
                pos,
            }
        })
        .collect()
}
fn ensure_states_exist_for_timestep(
    board: &Board,
    blizard_states_per_timestep: &mut HashMap<i64, HashSet<Blizzard>>,
    occupied_positions_per_timestep: &mut HashMap<i64, HashSet<Position>>,
    timestep: i64,
) {
    if let Some(_) = blizard_states_per_timestep.get(&timestep) {
        return;
    }
    // Assume previous state exists.
    let old_blizzards = blizard_states_per_timestep.get(&(timestep - 1)).unwrap();
    let new_blizzards = evolve_blizzards(board, &old_blizzards);
    let new_blizzards_pos: HashSet<_> = new_blizzards
        .iter()
        .map(|blizzard| blizzard.pos.clone())
        .collect();
    blizard_states_per_timestep.insert(timestep, new_blizzards);
    occupied_positions_per_timestep.insert(timestep, new_blizzards_pos);
}
#[derive(Debug, Hash, Clone, PartialEq, Eq)]
struct ScoredAttempt {
    attempt: Attempt,
    f: i64,
}
#[derive(Debug, Hash, Clone, PartialEq, Eq)]
struct Attempt {
    timestep: i64,
    position: Position,
}

fn manhattan_distance(from_: &Position, to_: &Position) -> i64 {
    (from_.x - to_.x).abs() + (from_.y - to_.y).abs()
}

impl PartialOrd for ScoredAttempt {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        other.f.partial_cmp(&self.f)
    }
}

impl Ord for ScoredAttempt {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.f.cmp(&self.f)
    }
}

fn part_one(board: &Board) -> i64 {
    let mut blizzard_states_per_timestep: HashMap<i64, HashSet<Blizzard>> = HashMap::new();
    blizzard_states_per_timestep.insert(0, board.blizzards.clone());
    let positions = board
        .blizzards
        .iter()
        .map(|blizzard| blizzard.pos.clone())
        .collect();
    let mut occupied_positions_per_timestep: HashMap<i64, HashSet<Position>> = HashMap::new();
    occupied_positions_per_timestep.insert(0, positions);
    let mut open_set: BinaryHeap<ScoredAttempt> = BinaryHeap::new();
    let mut already_attempted_scores: HashMap<Attempt, i64> = HashMap::new();
    open_set.push(ScoredAttempt {
        attempt: Attempt {
            timestep: 0,
            position: board.start_pos.clone(),
        },
        f: manhattan_distance(&board.start_pos, &board.end_pos),
    });
    loop {
        if let Some(scored_attempt) = open_set.pop() {
            let attempt = scored_attempt.attempt;
            if attempt.position == board.end_pos {
                return attempt.timestep;
            }
            if (attempt.position.x <= board.min_x
                || attempt.position.y <= board.min_y
                || attempt.position.x >= board.max_x
                || attempt.position.y >= board.max_y)
                && attempt.position != board.start_pos
            {
                continue;
            }
            ensure_states_exist_for_timestep(
                board,
                &mut blizzard_states_per_timestep,
                &mut occupied_positions_per_timestep,
                attempt.timestep + 1,
            );
            let occupied_positions = occupied_positions_per_timestep
                .get(&(attempt.timestep + 1))
                .unwrap();
            // Atempt to go in all directions
            let up_pos = Position {
                x: attempt.position.x,
                y: attempt.position.y - 1,
            };
            let up_attempt = ScoredAttempt {
                attempt: Attempt {
                    timestep: attempt.timestep + 1,
                    position: up_pos.clone(),
                },
                f: attempt.timestep + 1 + manhattan_distance(&board.end_pos, &up_pos),
            };
            let down_pos = Position {
                x: attempt.position.x,
                y: attempt.position.y + 1,
            };
            let down_attempt = ScoredAttempt {
                attempt: Attempt {timestep: attempt.timestep + 1,
                position: down_pos.clone()},
                f: attempt.timestep + 1 + manhattan_distance(&board.end_pos, &down_pos),
            };
            let left_pos = Position {
                x: attempt.position.x - 1,
                y: attempt.position.y,
            };
            let left_attempt = ScoredAttempt {
                attempt: Attempt{timestep: attempt.timestep + 1,
                position: left_pos.clone()},
                f: attempt.timestep + 1 + manhattan_distance(&board.end_pos, &left_pos),
            };
            let right_pos = Position {
                x: attempt.position.x + 1,
                y: attempt.position.y,
            };
            let right_attempt = ScoredAttempt {
                attempt: Attempt {timestep: attempt.timestep + 1,
                    position: right_pos.clone()},
                f: attempt.timestep + 1 + manhattan_distance(&board.end_pos, &right_pos),
            };
            let wait_attempt = ScoredAttempt {
                attempt: Attempt{timestep: attempt.timestep + 1,
                position: attempt.position.clone()},
                f: scored_attempt.f + 1 + manhattan_distance(&board.end_pos, &attempt.position),
            };
            let new_attempts = vec![
                up_attempt,
                down_attempt,
                left_attempt,
                right_attempt,
                wait_attempt,
            ];
            for new_attempt in new_attempts.into_iter() {
                // TODO: Add extra check for already attempted states.
                if !occupied_positions.contains(&new_attempt.attempt.position) {
                    let old_f = already_attempted_scores.get(&new_attempt.attempt).unwrap_or(&i64::MAX);
                    if *old_f > new_attempt.f {
                        already_attempted_scores.insert(new_attempt.attempt.clone(), new_attempt.f);
                        open_set.push(new_attempt);
                    }
                }
            }
        } else {
            break;
        }
    }
    panic!("Unable to reach a solution");
}

#[derive(Debug, Hash, Clone, PartialEq, Eq)]
struct PartTwoScoredAttempt {
    attempt: PartTwoAttempt,
    f: i64,
}
#[derive(Debug, Hash, Clone, PartialEq, Eq)]
struct PartTwoAttempt {
    timestep: i64,
    position: Position,
    leg_id: i64,
}

impl PartialOrd for PartTwoScoredAttempt {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        other.f.partial_cmp(&self.f)
    }
}

impl Ord for PartTwoScoredAttempt {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.f.cmp(&self.f)
    }
}
fn compute_heuristic_part_two(curr_pos: &Position, leg_id: i64, start_pos: &Position, end_pos: &Position) -> i64 {
    // Leg 0: start_pos => end_pos
    // Leg 1: end_pos => start_pos
    // Leg 2: start_pos => end_pos
    match leg_id {
        0 => manhattan_distance(curr_pos, end_pos) + 2 * manhattan_distance(start_pos, end_pos),
        1 => manhattan_distance(curr_pos, start_pos) + manhattan_distance(start_pos, end_pos),
        2 => manhattan_distance(curr_pos, end_pos),
        _ => unreachable!()
    }
}
fn part_two(board: &Board) -> i64 {
    let mut blizzard_states_per_timestep: HashMap<i64, HashSet<Blizzard>> = HashMap::new();
    blizzard_states_per_timestep.insert(0, board.blizzards.clone());
    let positions = board
        .blizzards
        .iter()
        .map(|blizzard| blizzard.pos.clone())
        .collect();
    let mut occupied_positions_per_timestep: HashMap<i64, HashSet<Position>> = HashMap::new();
    occupied_positions_per_timestep.insert(0, positions);
    let mut open_set: BinaryHeap<PartTwoScoredAttempt> = BinaryHeap::new();
    let mut already_attempted_scores: HashMap<PartTwoAttempt, i64> = HashMap::new();
    open_set.push(PartTwoScoredAttempt {
        attempt: PartTwoAttempt {
            timestep: 0,
            position: board.start_pos.clone(),
            leg_id: 0,

        },
        f: 3 * manhattan_distance(&board.start_pos, &board.end_pos),
    });
    loop {
        if let Some(scored_attempt) = open_set.pop() {
            let attempt = scored_attempt.attempt;
            if attempt.position == board.end_pos {
                if attempt.leg_id == 2 {
                    return attempt.timestep;
                }
            } else if attempt.position == board.start_pos {
                // Do nothing...
            } else if attempt.position.x <= board.min_x
                || attempt.position.y <= board.min_y
                || attempt.position.x >= board.max_x
                || attempt.position.y >= board.max_y
            {
                continue;
            }
            ensure_states_exist_for_timestep(
                board,
                &mut blizzard_states_per_timestep,
                &mut occupied_positions_per_timestep,
                attempt.timestep + 1,
            );
            let occupied_positions = occupied_positions_per_timestep
                .get(&(attempt.timestep + 1))
                .unwrap();
            // Atempt to go in all directions
            let up_pos = Position {
                x: attempt.position.x,
                y: attempt.position.y - 1,
            };
            let up_attempt = PartTwoScoredAttempt {
                attempt: PartTwoAttempt {
                    timestep: attempt.timestep + 1,
                    position: up_pos.clone(),
                    leg_id: attempt.leg_id,
                },
                f: attempt.timestep + 1 + compute_heuristic_part_two(&up_pos, attempt.leg_id, &board.start_pos, &board.end_pos),
            };
            let down_pos = Position {
                x: attempt.position.x,
                y: attempt.position.y + 1,
            };
            let down_attempt = PartTwoScoredAttempt {
                attempt: PartTwoAttempt {timestep: attempt.timestep + 1,
                    leg_id: attempt.leg_id,
                position: down_pos.clone()},
                f: attempt.timestep + 1 + compute_heuristic_part_two(&down_pos, attempt.leg_id, &board.start_pos, &board.end_pos),
            };
            let left_pos = Position {
                x: attempt.position.x - 1,
                y: attempt.position.y,
            };
            let left_attempt = PartTwoScoredAttempt {
                attempt: PartTwoAttempt{timestep: attempt.timestep + 1,
                    leg_id: attempt.leg_id,
                position: left_pos.clone()},
                f: attempt.timestep + 1 + compute_heuristic_part_two(&left_pos, attempt.leg_id, &board.start_pos, &board.end_pos),
            };
            let right_pos = Position {
                x: attempt.position.x + 1,
                y: attempt.position.y,
            };
            let right_attempt = PartTwoScoredAttempt {
                attempt: PartTwoAttempt {timestep: attempt.timestep + 1,
                    leg_id: attempt.leg_id,
                    position: right_pos.clone()},
                    f: attempt.timestep + 1 + compute_heuristic_part_two(&right_pos, attempt.leg_id, &board.start_pos, &board.end_pos),
            };
            let wait_attempt = PartTwoScoredAttempt {
                attempt: PartTwoAttempt{timestep: attempt.timestep + 1,
                    leg_id: attempt.leg_id,
                position: attempt.position.clone()},
                f: attempt.timestep + 1 + compute_heuristic_part_two(&attempt.position, attempt.leg_id, &board.start_pos, &board.end_pos),
            };
            let new_attempts = vec![
                up_attempt,
                down_attempt,
                left_attempt,
                right_attempt,
                wait_attempt,
            ];
            for new_attempt in new_attempts.into_iter() {
                let mut new_attempt = new_attempt;
                if new_attempt.attempt.position == board.end_pos {
                    if new_attempt.attempt.leg_id == 0 {
                        new_attempt.attempt.leg_id = 1;
                        new_attempt.f = new_attempt.attempt.timestep + compute_heuristic_part_two(&new_attempt.attempt.position, new_attempt.attempt.leg_id, &board.start_pos, &board.end_pos);
                    }
                } else if new_attempt.attempt.position == board.start_pos {
                    if new_attempt.attempt.leg_id == 1 {
                        new_attempt.attempt.leg_id = 2;
                        new_attempt.f = new_attempt.attempt.timestep + compute_heuristic_part_two(&new_attempt.attempt.position, new_attempt.attempt.leg_id, &board.start_pos, &board.end_pos);
                    }
                }
                // TODO: Add extra check for already attempted states.
                if !occupied_positions.contains(&new_attempt.attempt.position) {
                    let old_f = already_attempted_scores.get(&new_attempt.attempt).unwrap_or(&i64::MAX);
                    if *old_f > new_attempt.f {
                        already_attempted_scores.insert(new_attempt.attempt.clone(), new_attempt.f);
                        open_set.push(new_attempt);
                    }
                }
            }
        } else {
            break;
        }
    }
    panic!("Unable to reach a solution");
}

fn main() {
    let board = read_board(include_str!("./input.txt"));
    println!("{}", part_one(&board));
    println!("{}", part_two(&board));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part_one() {
        let board = read_board(include_str!("./test_input.txt"));
        assert_eq!(part_one(&board), 18);
    }
    #[test]
    fn test_part_two() {
        let board = read_board(include_str!("./test_input.txt"));
        assert_eq!(part_two(&board), 54);
    }
}
