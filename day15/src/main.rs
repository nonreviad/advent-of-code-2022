use std::{collections::HashSet, cmp::{max, min}};
use itertools::Itertools;
use rust_lapper::{Interval, Lapper};
use lazy_regex::{lazy_regex, Lazy, Regex};

static RE: Lazy<Regex> = lazy_regex!(
    r"Sensor at x=(?P<sensor_x>-?\d+), y=(?P<sensor_y>-?\d+): closest beacon is at x=(?P<beacon_x>-?\d+), y=(?P<beacon_y>-?\d+)"
);

#[derive(Debug, PartialEq, Eq)]
struct Pos {
    x: i64,
    y: i64,
}

#[derive(Debug)]
struct Sensor {
    pos: Pos,
    nearest_beacon: Pos,
    beacon_dist: u64,
}

impl Sensor {
    fn new(sensor_x: i64, sensor_y: i64, beacon_x: i64, beacon_y: i64) -> Self {
        let beacon_dist = ((sensor_x - beacon_x).abs() + (sensor_y - beacon_y).abs()) as u64;
        Self {
            beacon_dist,
            pos: Pos {
                x: sensor_x,
                y: sensor_y,
            },
            nearest_beacon: Pos {
                x: beacon_x,
                y: beacon_y,
            },
        }
    }
}

fn impossible_beacons(sensors: &Vec<Sensor>, y: i64) -> u64 {
    let xs: HashSet<(i64, i64)> = sensors
        .iter()
        .map(|sensor| {
            let extra_dist = sensor.beacon_dist as i64 - (sensor.pos.y - y).abs();
            if extra_dist > 0 {
                // [sensor_x, sensor_y] interval means the second needs to be +1
                Some((sensor.pos.x - extra_dist, sensor.pos.x + extra_dist + 1))
            } else {
                None
            }
        })
        .filter(|x| x.is_some())
        .map(|x| x.unwrap())
        .collect();
    let to_deduct = xs.iter().map(|x| x.0).min().unwrap();
    let xs = xs.iter().map(|(start, stop)| {
        let start = (*start - to_deduct) as u64;
        let stop = (*stop - to_deduct) as u64;
        Interval { start, stop, val: true}
    }).collect::<Vec<_>>();
    let mut lapper = Lapper::new(xs);
    lapper.merge_overlaps();
    lapper.cov() - sensors
        .iter()
        .filter(|sensor| sensor.nearest_beacon.y == y && sensor.nearest_beacon.x > to_deduct)
        .map(|sensor| sensor.nearest_beacon.x)
        .unique()
        .map(|x| {
            let x = (x - to_deduct) as u64;
            lapper.count(x, x + 1) as u64
        })
        .sum::<u64>()
}

fn tuning_frequency(sensors: &Vec<Sensor>, range: i64) -> u64 {
    let (x, y) = (0..=range).map(|x| {
        let mut pre_allocated: Vec<Interval<u64, bool>> = Vec::with_capacity(sensors.len());
        sensors.iter()
            .map(|sensor| {
                let x_delta = sensor.beacon_dist as i64 - (sensor.pos.x - x).abs();
                (sensor.pos.y - x_delta, sensor.pos.y + x_delta)
            })
            .filter(|(start, stop)| {
                (start <= stop) && (*start <= range as i64) && (*stop >= 0)
            })
            .for_each(|(start, stop)| {
                pre_allocated.push(Interval {start: max(0, start) as u64, stop: min(stop, range) as u64 + 1, val: true});
            });
        let mut k = Lapper::new(pre_allocated);
        k.merge_overlaps();
        if k.cov() == range as u64 {
            // A bit silly, I'm assuming it's not 0.
            (x, k.intervals[0].stop)
        } else {
            (-1, 0)
        }
    })
    .filter(|(start, _)| *start != -1)
    .exactly_one().unwrap();
    x as u64 * 4000000 + y
}

fn read_sensors(input: &str) -> Vec<Sensor> {
    input
        .lines()
        .map(|line| {
            let captures = RE
                .captures(line)
                .unwrap_or_else(|| panic!("Invalid match for {}", line));
            let sensor_x = captures
                .name("sensor_x")
                .unwrap_or_else(|| panic!("No sensor_x capture group!"))
                .as_str()
                .parse::<i64>()
                .unwrap_or_else(|e| panic!("Not a number. {}", e));
            let sensor_y = captures
                .name("sensor_y")
                .unwrap_or_else(|| panic!("No sensor_y capture group!"))
                .as_str()
                .parse::<i64>()
                .unwrap_or_else(|e| panic!("Not a number. {}", e));
            let beacon_x = captures
                .name("beacon_x")
                .unwrap_or_else(|| panic!("No beacon_x capture group!"))
                .as_str()
                .parse::<i64>()
                .unwrap_or_else(|e| panic!("Not a number. {}", e));
            let beacon_y = captures
                .name("beacon_y")
                .unwrap_or_else(|| panic!("No beacon_y capture group!"))
                .as_str()
                .parse::<i64>()
                .unwrap_or_else(|e| panic!("Not a number. {}", e));
            Sensor::new(sensor_x, sensor_y, beacon_x, beacon_y)
        })
        .collect()
}

fn main() {
    let sensors = read_sensors(include_str!("./input.txt"));
    println!("{}", impossible_beacons(&sensors, 2000000));
    println!("{}", tuning_frequency(&sensors, 4000000));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_read() {
        let sensors = read_sensors(include_str!("./test_input.txt"));
        assert_eq!(sensors.len(), 14);
    }

    #[test]
    fn test_part_one() {
        let sensors = read_sensors(include_str!("./test_input.txt"));
        assert_eq!(impossible_beacons(&sensors, 10), 26);
    }

    #[test]
    fn test_part_one_real() {
        let sensors = read_sensors(include_str!("./input.txt"));
        assert_eq!(impossible_beacons(&sensors, 2000000), 5394423);
    }

    #[test]
    fn test_part_two() {
        let sensors = read_sensors(include_str!("./test_input.txt"));
        assert_eq!(tuning_frequency(&sensors, 20), 56000011);
    }
    #[test]
    fn test_part_two_real() {
        let sensors = read_sensors(include_str!("./input.txt"));
        assert_eq!(tuning_frequency(&sensors, 4000000), 11840879211051);
    }
}
