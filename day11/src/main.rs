use std::{str::Lines, collections::HashMap};
use itertools::Itertools;

#[derive(Debug)]
enum Op {
    Add(u64),
    Mult(u64),
    Square
}

impl Op {
    fn apply(&self, val: u64) -> u64 {
        match self {
            Op::Add(x) => val + x,
            Op::Mult(x) => val * x,
            Op::Square => val * val,
        }
    }
}

impl From<&str> for Op {
    fn from(s: &str) -> Self {
        let (_, op) = s.split_once(':').unwrap();
        let (_, op) = op.split_once('=').unwrap();
        let (_, op) = op.split_once("old").unwrap();
        let op = op.trim();
        let (op, val) = op.split_once(' ').unwrap();
        match (op, val) {
            ("*", "old") => Op::Square,
            ("*", _) => Op::Mult(val.parse::<u64>().unwrap()),
            ("+", _) => Op::Add(val.parse::<u64>().unwrap()),
            (_, _) => panic!("Invalid {op} {val}")
        }
    }
}
#[derive(Debug)]
struct Test {
    mod_val: u64,
    true_branch: usize,
    false_branch: usize,
}

impl Test {
    fn apply(&self, val: u64) -> usize {
        if val % self.mod_val == 0 {
            self.true_branch
        } else {
            self.false_branch
        }
    }
}

impl Test {
    fn from(lines: &mut Lines) -> Self {
        let (_, mod_val) = lines.next().unwrap().rsplit_once(' ').unwrap();
        let mod_val = mod_val.parse::<u64>().unwrap();
        let (_, true_branch) = lines.next().unwrap().rsplit_once(' ').unwrap();
        let true_branch = true_branch.parse::<usize>().unwrap();
        let (_, false_branch) = lines.next().unwrap().rsplit_once(' ').unwrap();
        let false_branch = false_branch.parse::<usize>().unwrap();
        Self {mod_val, true_branch, false_branch}
    }
}
#[derive(Debug)]
struct Monkey {
    id: usize,
    objects: Vec<u64>,
    modifier: Op,
    test: Test,
    inspected: usize
}

impl Monkey {
    fn inspect(&mut self, others: &mut HashMap<usize, Monkey>) {
        self.inspected += self.objects.len();
        for worry in self.objects.iter() {
            let worry = self.modifier.apply(*worry);
            let worry = worry / 3;
            let next_monkey = self.test.apply(worry);
            if let Some(monkey) = others.get_mut(&next_monkey) {
                monkey.objects.push(worry);
            }
        }
        self.objects.clear();
    }
    fn inspect_v2(&mut self, mega_mod: u64, others: &mut HashMap<usize, Monkey>) {
        self.inspected += self.objects.len();
        for worry in self.objects.iter() {
            let worry = self.modifier.apply(*worry) % mega_mod;
            let next_monkey = self.test.apply(worry);
            if let Some(monkey) = others.get_mut(&next_monkey) {
                monkey.objects.push(worry);
            }
        }
        self.objects.clear();
    }
}


impl From<&str> for Monkey {
    fn from(s: &str) -> Self {
        let mut lines = s.lines();
        let id_line = lines.next().unwrap();
        let (_, id) = id_line.split_once(' ').unwrap();
        let (id,_) = id.split_once(':').unwrap();
        let id = id.parse::<usize>().unwrap();
        let (_, objects) = lines.next().unwrap().split_once(':').unwrap();
        let objects = objects.split(',').map(|item| item.trim().parse::<u64>().unwrap()).collect::<Vec<_>>();
        let modifier = Op::from(lines.next().unwrap());
        let test = Test::from(&mut lines);
        Self {
            id, objects, modifier, test, inspected:0
        }
    }
}

fn main() {
    let input = include_str!("./input.txt");
    let mut monkeys = input.split("\n\n").map(|monkey_lines| {
        let monkey = Monkey::from(monkey_lines);
        (monkey.id, monkey)
    }).collect::<HashMap<usize, Monkey>>();
    let num_monkeys = monkeys.len();
    for _ in 0..20 {
        for id in 0..num_monkeys {
            let mut monkey = monkeys.remove(&id).unwrap();
            monkey.inspect(&mut monkeys);
            monkeys.insert(monkey.id, monkey);
        }
    }
    let mut part_one = monkeys.values().map(|monkey| monkey.inspected).collect::<Vec<usize>>();
    part_one.sort();
    part_one.reverse();
    let part_one = part_one.iter().take(2).product::<usize>();
    println!("{}", part_one);


    let mut monkeys = input.split("\n\n").map(|monkey_lines| {
        let monkey = Monkey::from(monkey_lines);
        (monkey.id, monkey)
    }).collect::<HashMap<usize, Monkey>>();
    let mega_mod = monkeys.values().map(|monkey| monkey.test.mod_val).unique().product::<u64>();
    let num_monkeys = monkeys.len();
    for _ in 0..10000 {
        for id in 0..num_monkeys {
            let mut monkey = monkeys.remove(&id).unwrap();
            monkey.inspect_v2(mega_mod, &mut monkeys);
            monkeys.insert(monkey.id, monkey);
        }
    }
    let mut part_two = monkeys.values().map(|monkey| monkey.inspected).collect::<Vec<usize>>();
    part_two.sort();
    part_two.reverse();
    let part_two = part_two.iter().take(2).product::<usize>();
    println!("{}", part_two);
}
