struct Interval {
    start: u32,
    end: u32
}

impl Interval {
    fn includes(&self, other: &Self) -> bool {
        other.start >= self.start && other.end <= self.end
    }
    fn overlap(&self, other: &Self) -> bool {
        (other.start >= self.start && other.start <= self.end) ||
        (other.end >= self.start && other.end <= self.end) ||
        (self.start >= other.start && self.start <= other.end) ||
        (self.end >= other.start && self.end <= other.end)
    }
}

impl From<&str> for Interval {
    fn from(val: &str) -> Interval {
        let (start, end) = val.split_once('-').expect("No - character for elves!");
        let start = start.parse::<u32>().expect("Invalid start range");
        let end = end.parse::<u32>().expect("Invalid start range");
        Interval { start, end }
    }
}

fn main() {
    let input = include_str!("./input.txt");
    let part_one = input
        .lines()
        .map(|line| {
            let (elf1, elf2) = line.split_once(',').expect("Should contain two elves per line");
            let elf1 = Interval::from(elf1);
            let elf2 = Interval::from(elf2);
            if elf1.includes(&elf2) || elf2.includes(&elf1) {
                1
            } else {
                0
            }
        })
        .sum::<u32>();
    let part_two = input
        .lines()
        .map(|line| {
            let (elf1, elf2) = line.split_once(',').expect("Should contain two elves per line");
            let elf1 = Interval::from(elf1);
            let elf2 = Interval::from(elf2);
            if elf1.overlap(&elf2) {
                1
            } else {
                0
            }
        })
        .sum::<u32>();
    println!("{}", part_one);
    println!("{}", part_two);
}
