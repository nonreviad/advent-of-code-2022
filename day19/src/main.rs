#![allow(dead_code)]

use lazy_regex::{lazy_regex, Lazy, Regex};
use std::{
    cmp::{max, min},
    collections::{BinaryHeap, HashMap, HashSet},
};

#[derive(Hash, Eq, PartialEq, Clone, Debug, PartialOrd, Ord)]
enum Mineral {
    Ore,
    Clay,
    Obsidian,
    Geode,
}

impl Mineral {
    fn from_usize(s: usize) -> Self {
        match s {
            0 => Mineral::Ore,
            1 => Mineral::Clay,
            2 => Mineral::Obsidian,
            3 => Mineral::Geode,
            _ => panic!(""),
        }
    }
    fn to_usize(&self) -> usize {
        match self {
            Mineral::Ore => 0,
            Mineral::Clay => 1,
            Mineral::Obsidian => 2,
            Mineral::Geode => 3,
        }
    }
}

impl From<&str> for Mineral {
    fn from(s: &str) -> Self {
        match s {
            "ore" => Self::Ore,
            "clay" => Self::Clay,
            "obsidian" => Self::Obsidian,
            "geode" => Self::Geode,
            _ => panic!("Invalid mineral type {s}"),
        }
    }
}

static RECIPE_RE: Lazy<Regex> =
    lazy_regex!(r" Each (?P<result>(ore|clay|obsidian|geode)) robot costs (?P<costs>[^\.]+)\.");

#[derive(Debug)]
struct Recipe {
    result: Mineral,
    costs: [u32; 4],
}

#[derive(Debug)]
struct Blueprint {
    id: u32,
    recipes: HashMap<Mineral, Recipe>,
}

impl From<&str> for Blueprint {
    fn from(s: &str) -> Self {
        let (before, after) = s.split_once(':').unwrap();
        let (_, id) = before.split_once(' ').unwrap();
        let id = id.parse::<u32>().unwrap();
        let recipes: HashMap<Mineral, Recipe> = RECIPE_RE
            .captures_iter(after)
            .map(|capture| {
                let result = Mineral::from(capture.name("result").unwrap().as_str());
                let mut costs = [0; 4];
                capture
                    .name("costs")
                    .unwrap()
                    .as_str()
                    .split(" and ")
                    .for_each(|cost| {
                        let (quantity, ingredient) = cost.split_once(' ').unwrap();
                        let quantity = quantity.parse::<u32>().unwrap();
                        let ingredient = Mineral::from(ingredient);
                        costs[ingredient.to_usize()] = quantity;
                    });
                (result.clone(), Recipe { result, costs })
            })
            .collect();
        Self { id, recipes }
    }
}

#[derive(PartialEq, Eq, Clone, Hash)]
struct PartialSolution {
    remaining_time: u32,
    robots: [u32; 4],
    ore: [u32; 4],
}

impl PartialSolution {
    fn new(remaining_time: u32) -> Self {
        let robots = [0; 4];
        let ore = [0; 4];
        Self {
            remaining_time,
            robots,
            ore,
        }
    }
    fn increase_ore(&mut self, ore: &Mineral, quantity: u32) {
        self.ore[ore.to_usize()] += quantity;
    }
    fn decrease_ore(&mut self, ore: &Mineral, quantity: u32) {
        self.ore[ore.to_usize()] -= quantity;
    }
    fn increase_bot(&mut self, ore: &Mineral, quantity: u32) {
        self.robots[ore.to_usize()] += quantity;
    }
    fn apply_recipe(&self, recipe: &Recipe) -> Option<Self> {
        if self.remaining_time > 0
            && recipe
                .costs
                .iter()
                .enumerate()
                .all(|(k, v)| self.ore[k] >= *v)
        {
            let mut result = self.progress().unwrap();
            recipe.costs.iter().enumerate().for_each(|(ore, quantity)| {
                result.decrease_ore(&Mineral::from_usize(ore), *quantity)
            });
            result.increase_bot(&recipe.result, 1);
            Some(result)
        } else {
            None
        }
    }
    fn progress(&self) -> Option<Self> {
        if self.remaining_time == 0 {
            None
        } else {
            let mut new_state = self.clone();
            new_state.remaining_time -= 1;
            self.robots.iter().enumerate().for_each(|(ore, quantity)| {
                new_state.increase_ore(&Mineral::from_usize(ore), *quantity)
            });
            Some(new_state)
        }
    }
}

impl Ord for PartialSolution {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let c1 = self.ore[Mineral::Geode.to_usize()].cmp(&other.ore[Mineral::Geode.to_usize()]);
        if c1 == std::cmp::Ordering::Equal {
            return self.remaining_time.cmp(&other.remaining_time);
        }
        c1
    }
}

impl PartialOrd for PartialSolution {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Blueprint {
    fn quality_level(&self, max_timeout: u32) -> u32 {
        let mut pq: BinaryHeap<PartialSolution> = BinaryHeap::new();
        let mut start_state = PartialSolution::new(max_timeout);
        start_state.increase_bot(&Mineral::Ore, 1);
        let mut explored: HashSet<PartialSolution> = HashSet::new();
        let mut maxes: [u32; 3] = [0; 3];
        for recipe in self.recipes.values() {
            for mineral in 0..3 {
                maxes[mineral] = max(maxes[mineral], recipe.costs[mineral]);
            }
        }
        explored.insert(start_state.clone());
        pq.push(start_state);
        let mut best_count = 0;
        while let Some(state) = pq.pop() {
            if state.remaining_time == 0 {
                best_count = max(best_count, state.ore[Mineral::Geode.to_usize()]);
            }
            // Do nothing, just mine
            if state.remaining_time > 1 && state.apply_recipe(self.recipes.get(&Mineral::Geode).unwrap()).is_none() {
                if let Some(next_state) = state.progress() {
                    if !explored.contains(&next_state) {
                        pq.push(next_state.clone());
                        explored.insert(next_state);
                    }
                }
            }
            // Try recipes
            self.recipes.iter().for_each(|(_, recipe)| {
                if recipe.result == Mineral::Geode
                    || state.robots[recipe.result.to_usize()] < maxes[recipe.result.to_usize()]
                {
                    if let Some(next_state) = state.apply_recipe(recipe) {
                        if !explored.contains(&next_state) {
                            pq.push(next_state.clone());
                            explored.insert(next_state);
                        }
                    }
                }
            });
        }
        best_count
    }
}

fn blueprints(s: &str) -> Vec<Blueprint> {
    s.lines().map(|line| Blueprint::from(line)).collect()
}

fn part_one(s: &str) -> u32 {
    let bps = blueprints(s);
    bps.iter().map(|bp| bp.quality_level(24) * bp.id).sum::<u32>()
}

fn part_two(s: &str) -> u32 {
    let bps = blueprints(s);
    bps.iter().take(min(3, bps.len())).map(|bp| bp.quality_level(32)).product::<u32>()
}

fn main() {
    println!("{}", part_one(include_str!("./input.txt")));
    println!("{}", part_two(include_str!("./input.txt")));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_part_one() {
        assert_eq!(part_one(include_str!("./test_input.txt")), 33);
    }
    #[test]
    fn test_part_two() {
        assert_eq!(part_two(include_str!("./test_input.txt")), 56*62);
    }
}
