use std::collections::HashSet;

use itertools::Itertools;
type Pos = (u32, u32);

fn drop_sand(at_pos: &Pos, floor: u32, structure: &HashSet<Pos>) -> Option<Pos> {
    let mut pos = (at_pos.0, at_pos.1);
    loop {
        if structure.contains(&pos) {
            return None;
        }
        if pos.1 >= floor {
            return None;
        }
        let down  = (pos.0 + 0, pos.1 + 1);
        let left  = (pos.0 - 1, pos.1 + 1);
        let right = (pos.0 + 1, pos.1 + 1);

        if !structure.contains(&down) {
            pos = down;
        } else if !structure.contains(&left) {
            pos = left;
        } else if !structure.contains(&right) {
            pos = right;
        } else {
            return Some(pos);
        }
    }
}

fn drop_sand_v2(at_pos: &Pos, floor: u32, structure: &HashSet<Pos>) -> Option<Pos> {
    let mut pos = (at_pos.0, at_pos.1);
    loop {
        if structure.contains(&pos) {
            return None;
        }
        if pos.1 == floor - 1 {
            return Some(pos);
        }
        let down  = (pos.0 + 0, pos.1 + 1);
        let left  = (pos.0 - 1, pos.1 + 1);
        let right = (pos.0 + 1, pos.1 + 1);

        if !structure.contains(&down) {
            pos = down;
        } else if !structure.contains(&left) {
            pos = left;
        } else if !structure.contains(&right) {
            pos = right;
        } else {
            return Some(pos);
        }
    }
}

fn read_rocks() -> HashSet<Pos> {
    include_str!("./input.txt").lines().flat_map(|line| {
        let segments: Vec<(u32, u32)> = line.split(" -> ").map(|segment| {
            let (a, b) = segment.split_once(',').unwrap();
            (a.parse::<u32>().unwrap(), b.parse::<u32>().unwrap())
        }).collect();
        (0..segments.len() - 1).flat_map(move |idx| {
            let (a1,b1) = segments[idx];
            let (a2,b2) = segments[idx + 1];
            let (min_a, max_a) = (std::cmp::min(a1, a2), std::cmp::max(a1,a2));
            let (min_b, max_b) = (std::cmp::min(b1, b2), std::cmp::max(b1,b2));
            (min_a..=max_a).cartesian_product(min_b..=max_b)
        })
    }).collect()
}

fn main() {
    let mut rocks: HashSet<Pos> = read_rocks();
    let floor = *rocks.iter().map(|(_,y)| y).max().unwrap();
    let mut count = 0;
    loop {
        if let Some(new_pos) = drop_sand(&(500,0), floor, &rocks) {
            rocks.insert(new_pos);
            count += 1;
        } else {
            break;
        }
    }
    println!("{}", count);

    let mut rocks: HashSet<Pos> = read_rocks();
    let floor = *rocks.iter().map(|(_,y)| y).max().unwrap() + 2;
    let mut count = 0;
    loop {
        if let Some(new_pos) = drop_sand_v2(&(500,0), floor, &rocks) {
            rocks.insert(new_pos);
            count += 1;
        } else {
            break;
        }
    }
    println!("{}", count);
}
